﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ShoppingCartTraining.Common.Models
{
    public class ReturnModel
    {
        public int ReturnID { get; set; }
        public int PurchaseID { get; set; }
        public int ProductID { get; set; }
        public int ReturnedQuantity { get; set; }
        public decimal ReturnedTotalDiscount { get; set; }
        public decimal ReturnedTotalAmount { get; set; }
        public string ReturnedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal UnitDiscount { get; set; }
        public string PurchasedDate { get; set; }
        public int Quantity { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal TotalAmount { get; set; }
    }
    public class ReturnViewModel
    {
        public int PurchaseID { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string ReturnedDate { get; set; }
        public int ReturnedQuantity { get; set; }
        public decimal ReturnedTotalDiscount { get; set; }
        public decimal ReturnedTotalAmount { get; set; }
    }
}