import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import App from './App.vue';
import router from './router'; // To import a router
import "./assets/styles/app.scss"; // To import styles
// To import directly bootstrap
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VeeValidate from "vee-validate";

Vue.config.productionTip = false

Vue.use(BootstrapVue);
Vue.use(VeeValidate);

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
