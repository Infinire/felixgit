import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '../home/HomePage.vue';
import AddCustomer from '../customer/AddCustomer.vue';
import ListCustomers from '../customer/Index.vue';

Vue.use(Router);

export default new Router ({
    mode: 'history',
    routes : [
        {
            name        : 'Home',
            path        : '/',
            component   : HomePage
        },
        {
            name        : 'Customers',
            path        : '/customers',
            component   : ListCustomers
        },
        {
            name        : 'Add Customer',
            path        : '/customer/add',
            component   : AddCustomer
        }
    ],
});