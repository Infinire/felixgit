﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShoppingCartTraining.Business;
namespace ShoppingCartTraining.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchaseController : Controller
    {
        private readonly IPurchaseProvider _provider;
        public PurchaseController(IPurchaseProvider provider)
        {
            _provider = provider;
        }
        [HttpGet]
        [Route("PurchaseDetails")]
        public async Task<IActionResult> PurchaseDetails()
        {
            try
            {
                var purchase = await _provider.GetPurchaseDetails();
                return Ok(purchase);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpPost]
        [Route("CreatePurchaseDetails")]
        public async Task<IActionResult> CreatePurchaseDetails(int productID, int quantity, string createdBy)
        {
            try
            {
                var result = await _provider.CreatePurchaseDetails(productID, quantity, createdBy);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpDelete]
        [Route("DeletePurchaseByID")]
        public async Task<IActionResult> DeletePurchaseByID(int id)
        {
            try
            {
                var result = await _provider.DeletePurchaseDetailsById(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
    }
}
