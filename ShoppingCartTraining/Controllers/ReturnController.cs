﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShoppingCartTraining.Business;
namespace ShoppingCartTraining.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReturnController : Controller
    {
        private readonly IReturnProvider _provider;
        public ReturnController(IReturnProvider provider)
        {
            _provider = provider;
        }
        [HttpGet]
        [Route("GetReturnDetails")]
        public async Task<IActionResult> GetReturnDetails()
        {
            try
            {
                var result = await _provider.ReturnDetails();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpPost]
        [Route("ReturnProduct")]
        public async Task<IActionResult> ReturnProduct(int purchaseID, int productID, int returnedQuantity)
        {
            try
            {
                var returnProducts = await _provider.ReturnProducts(purchaseID, productID, returnedQuantity);
                return Ok(returnProducts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpDelete]
        [Route("DeleteReturnItemsByPurchaseID")]
        public async Task<IActionResult> DeleteReturnItemsByPurchaseID(int id)
        {
            try
            {
                var ReturnProductDelte = await _provider.DeleteReturnItemsByPurchaseID(id);
                return Ok(ReturnProductDelte);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpGet]
        [Route("GetPurchaseDetails")]
        public async Task<IActionResult> GetPurchaseDetails()
        {
            try
            {
                var ReturnedProduct = await _provider.GetPurchaseDetails();
                return Ok(ReturnedProduct);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
    }
}
