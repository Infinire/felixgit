﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShoppingCartTraining.Business;
using ShoppingCartTraining.Common.Models;
namespace ShoppingCartTraining.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IProductProvider _provider;
        public ProductController(IProductProvider provider)
        {
            _provider = provider;
        }
        [HttpGet]
        [Route("GeProducts")]
        public async Task<IActionResult> GetProducts()
        {
            try 
            {
                var products = await _provider.GetProducts();
                return Ok(products);
            } 
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpGet]
        [Route("GetProductById/{productId}")]
        public async Task<IActionResult> Get(int productId)
        {
            var products = await _provider.GetProductById(productId);
            return Ok(products);
        }
        [HttpPost]
        [Route("CreateProduct")]
        public async Task<IActionResult> CreateProduct([FromBody] ProductModel data)
        {
            try
            {
                if (!(data.UnitDiscount >= 0))
                {
                    throw new Exception("Unit Discount accepts 0 or more rupees");
                }
                if (!(data.UnitPrice > 0))
                {
                    throw new Exception("Unit price must be greater than zero");
                }
                if (data.CreatedBy == null)
                {
                    throw new Exception("CreatedBy field is required");
                }
                var products = await _provider.CreateProduct(data);
                return Ok(products);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpPost]
        [Route("UpdateProduct")]
        public async Task<IActionResult> UpdateProduct([FromBody] ProductModel data)
        {
            try
            {
                if (!(data.UnitDiscount >= 0))
                {
                    throw new Exception("Unit Discount accepts 0 or more rupees");
                }
                if (!(data.UnitPrice > 0))
                {
                    throw new Exception("Unit price must be greater than zero");
                }
                if (string.IsNullOrEmpty(data.ModifiedBy))
                {
                    throw new Exception("ModifiedBy field is required");
                }

                var products = await _provider.UpdateProduct(data);
                return Ok(products);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpDelete]
        [Route("DeleteProduct")]
        public async Task<IActionResult> DeleteProduct(int productId)
        {
            try 
            {
                var result = await _provider.DeleteProduct(productId);
                return Ok("Deleted successfully!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
    }
}