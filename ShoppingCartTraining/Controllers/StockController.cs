﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShoppingCartTraining.Business;
namespace ShoppingCartTraining.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockController : Controller
    {
        private readonly IStockProvider _provider;
        public StockController(IStockProvider provider)
        {
            _provider = provider;
        }
        [HttpGet]
        [Route("GetStock")]
        public async Task<IActionResult> GetStock()
        {
            var stocks = await _provider.GetStocks();
            return Ok(stocks);
        }
        [HttpPost]
        [Route("CreateStockDetails")]
        public async Task<IActionResult> CreateStockDetails(int productID, int quantity)
        {
            try
            {
                var stocks = await _provider.CreateStockDetails(productID, quantity);
                return Ok(stocks);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpPost]
        [Route("UpdateStockDetailsById")]
        public async Task<IActionResult> UpdateStockDetailsById(int stockID, int quantity)
        {
            try
            {
                var stocks = await _provider.UpdateStockDetailsById(stockID, quantity);
                return Ok(stocks);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpDelete]
        [Route("DeleteStockByID")]
        public ActionResult DeleteStockByID(int stockId)
        {
            try
            {
                var stocks = _provider.DeleteStockDetailsById(stockId);
                return Ok(stocks);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
    }
}