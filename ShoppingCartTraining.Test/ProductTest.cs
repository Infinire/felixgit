﻿using System.Threading.Tasks;
using NUnit.Framework;
using ShoppingCartTraining.Business;
using ShoppingCartTraining.Common.Models;
using Moq;
using ShoppingCartTraining.DataAccess;
using Microsoft.Extensions.Configuration;

namespace ShoppingCartTraining.Test
{
    [TestFixture]
    public class ProductTest
    {
        private IProductProvider _provider;
        private Mock<IProductDataAccess> _dataAccess;
        private IProductDataAccess _productDataAccess;
        private IProductProvider _productProvider;
        private IConfiguration _configuration;
        private ICommonDataAccess _commonDataAccess;
        private static Random random = new Random();

        [SetUp]
        public void Init()
        {
            //Mocking
            _dataAccess = new Mock<IProductDataAccess>();
            _provider = new ProductProvider(_dataAccess.Object);

            //For connecting the database
             var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddEnvironmentVariables();
            _configuration = builder.Build();

            _commonDataAccess = new CommonDataAccess();
            _productDataAccess = new ProductDataAccess(_configuration, _commonDataAccess);
            _productProvider = new ProductProvider(_productDataAccess);
        }

        [Test]
        public void SendProductId_FindProducts_ReturnProduct()
        {
            //Arrange
            //var products = new List<ProductModel>();
            //                products.Add(new ProductModel {
            //                    ProductID = 1,
            //                    ProductName = "Fan",
            //                    UnitPrice = 2000,
            //                    UnitDiscount = 10,
            //                    CreatedBy = "Felix"
            //                });

            //var products = new List<ProductModel>();
            //var a = new ProductModel
            //{
            //    ProductID = 1,
            //    ProductName = "Fan",
            //    UnitPrice = 2000,
            //    UnitDiscount = 10,
            //    CreatedBy = "Felix"
            //};
            //products.Add(a);

            var product = new List<ProductModel>
            {
                new ProductModel()
                {
                    ProductID = 1,
                    ProductName = "Fan",
                    UnitPrice = 2000,
                    UnitDiscount = 10,
                    CreatedBy = "Felix"
                }
            };

            //Act
            _dataAccess.Setup(x => x.GetProductById(1)).ReturnsAsync(product);
            var result = _provider.GetProductById(1).GetAwaiter().GetResult();

            //Assert
            Assert.IsTrue(result.Count() > 0);
        }

        [Test]
        public void SendProductID_FetchProduct_ReturnSuccess() //database connection
        {
            var result = _productProvider.GetProductById(1).GetAwaiter().GetResult();
            
            Assert.IsTrue(result.Count > 0);

        }

        [Test]
        public void GetProducts_MockResponseData_AssertSuccess()
        {
            //Arrange
            var products = new List<ProductModel>();
            products.Add( new ProductModel
            {
                ProductID = 1,
                ProductName = "Fan",
                UnitPrice = 2000,
                UnitDiscount = 10,
                CreatedBy = "Felix"
            });
            products.Add(new ProductModel
            {
                ProductID = 2,
                ProductName = "Fridge",
                UnitPrice = 10000,
                UnitDiscount = 10,
                CreatedBy = "Felix raj"
            });

            //Act
            _dataAccess.Setup(x => x.GetProducts()).ReturnsAsync(products);
            var result = _provider.GetProducts().GetAwaiter().GetResult();

            //Assert
            Assert.IsTrue(result.Count > 0);
        }

        [Test]
        public void SendProduct_SaveProduct_ReturnSuccessResponse()
        {
            var product = new ProductModel
            {
                ProductID = 1,
                ProductName = "Fridge" + RandomString(5),
                UnitPrice = 10000,
                UnitDiscount = 10,
                CreatedBy = "Felix raj"
            };

            var result = _productProvider.CreateProduct(product).GetAwaiter().GetResult();

            Assert.IsTrue(result.Count > 0);
        }

        [Test]
        public void SendProductId_DeleteProductIfExist_ReturnSuccess()
        {
            _dataAccess.Setup(x => x.DeleteProduct(1)).ReturnsAsync(true);
            var result = _provider.DeleteProduct(1).GetAwaiter().GetResult();

            //Assert
            Assert.AreEqual(true, result);

        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }


    }
}
