﻿using Moq;
using NUnit.Framework;
using ShoppingCartTraining.Business;
using ShoppingCartTraining.Common.Models;
using ShoppingCartTraining.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingCartTraining.Test
{
    [TestFixture]
    public class ReturnTest
    {
        private Mock<IReturnDataAccess> _dataAccess;
        private IReturnProvider _provider;

        [SetUp]
        public void Init()
        {
            _dataAccess = new Mock<IReturnDataAccess>();
            _provider = new ReturnProvider(_dataAccess.Object);
        }

        [Test]
        public void GetReturnDetails_NoParameters_ReturnSuccess()
        {
            var reutnsDetails = new List<ReturnViewModel>
            {
                new ReturnViewModel()
                {
                    PurchaseID= 6,
                    ProductID= 4,
                    ProductName= "Laptop",
                    ReturnedDate= "04-21-2022 23:33:48 +05:30",
                    ReturnedQuantity= 1,
                    ReturnedTotalDiscount= 5000,
                    ReturnedTotalAmount= 45000
                }
            };
            _dataAccess.Setup(x => x.ReturnDetails()).ReturnsAsync(reutnsDetails);
            var result = _provider.ReturnDetails().GetAwaiter().GetResult();

            Assert.That(result, Is.Not.Null);
            Assert.IsTrue(result.Count > 0);
        }

        [Test]
        public void ReturnProucts_PassParameter_ReturnSuccess()
        {
            _dataAccess.Setup(x => x.ReturnProducts(1, 1, 5)).ReturnsAsync("The product returned successfully!");
            var result = _provider.ReturnProducts(1, 1, 5).GetAwaiter().GetResult();

            Assert.AreEqual("The product returned successfully!", result);
        }

        [Test]
        public void DeleteReturn_SendReturnIDParameter_ReturnSuccess()
        {
            _dataAccess.Setup(x => x.DeleteReturnItemsByPurchaseID(1)).ReturnsAsync("Deleted Successfully!");
            var result = _provider.DeleteReturnItemsByPurchaseID(1).GetAwaiter().GetResult();

            Assert.AreEqual("Deleted Successfully!", result);
        }

        [Test]
        public void GetPurchaseDetails_NoParameters_ReturnSuccess()
        {
            var purchaseDetails = new List<PurchaseDetailsModel>
            {
                new PurchaseDetailsModel()
                {
                    ProductID = 4,
                    ProductName = "Laptop",
                    PurchasedDate = "04-06-2022 15:56:03 +05:30",
                    Quantity = 5,
                    TotalDiscount = 25000,
                    TotalAmount = 225000,
                    ReturnedDate = "04-21-2022 23:33:48 +05:30",
                    ReturnedQuantity = 1,
                    ReturnedTotalDiscount = 5000,
                    ReturnedTotalAmount = 45000
                }
            };
            _dataAccess.Setup(x => x.GetPurchaseDetails()).ReturnsAsync(purchaseDetails);
            var result = _provider.GetPurchaseDetails().GetAwaiter().GetResult();
        }
    }
}
