﻿using Moq;
using NUnit.Framework;
using ShoppingCartTraining.Business;
using ShoppingCartTraining.Common.Models;
using ShoppingCartTraining.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingCartTraining.Test
{
    [TestFixture]
    public class StockTest
    {
        private IStockProvider _provider;
        private Mock<IStockDataAccess> _dataAccess;

        [SetUp]
        public void Init()
        {
            _dataAccess = new Mock<IStockDataAccess>();
            _provider = new StockProvider(_dataAccess.Object);
        }

        [Test]
        public void GetStocks_NoArgument_ReturnSuccess()
        {
            var Stocks = new List<StockModel>
            {
                new StockModel()
                {
                    StockID = 1,
                    ProductID =  4,
                    ProductName= "Laptop",
                    UnitPrice= 50000,
                    TotalAmout= 2000000,
                    Quantity= 40,
                    CreatedBy= null,
                    ModifiedBy= null,
                    CreatedDate= null,
                    ModifiedDate= null
                },
                new StockModel()
                {
                    StockID = 6,
                    ProductID = 1,
                    ProductName= "HardDisk",
                    UnitPrice= 3,
                    TotalAmout= 30,
                    Quantity= 10,
                    CreatedBy= null,
                    ModifiedBy= null,
                    CreatedDate= null,
                    ModifiedDate= null
                }
            };

            _dataAccess.Setup(x => x.GetStocks()).ReturnsAsync(Stocks);
            var result = _provider.GetStocks().GetAwaiter().GetResult();

            Assert.IsTrue(result.Count > 0);
        }

        [Test]
        public void SendProductIdQuantity_InsertStock_ReturnSuccess()
        {
            _dataAccess.Setup(x => x.CreateStockDetails(1, 10)).ReturnsAsync("Inserted Successfully!.");
            var result = _provider.CreateStockDetails(1, 10).GetAwaiter().GetResult();

            Assert.AreEqual("Inserted Successfully!.", result);
        }

        [Test]
        public void SendProductIdQuantity_UpdateStock_ReturnSuccess()
        {
            _dataAccess.Setup(x => x.CreateStockDetails(1, 10)).ReturnsAsync("Successfully updated!");
            var result = _provider.CreateStockDetails(1, 10).GetAwaiter().GetResult();

            Assert.AreEqual("Successfully updated!", result);
        }

        [Test]
        public void DeleteSotck_ByStockId_ReturnSuccess()
        {
            _dataAccess.Setup(x => x.DeleteStockDetailsById(1)).ReturnsAsync("Deleted successfully!");
            var result = _provider.DeleteStockDetailsById(1).GetAwaiter().GetResult();

            Assert.AreEqual("Deleted successfully!", result);
        }
    }
}
