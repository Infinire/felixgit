﻿using Moq;
using NUnit.Framework;
using ShoppingCartTraining.Business;
using ShoppingCartTraining.Common.Models;
using ShoppingCartTraining.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingCartTraining.Test
{
    [TestFixture]
    public class PurchaseTest
    {
        private Mock<IPurchaseDataAccess> _dataAccess;
        private IPurchaseProvider _provider;

        [SetUp]
        public void Init()
        {
            _dataAccess = new Mock<IPurchaseDataAccess>();
            _provider = new PurchaseProvider(_dataAccess.Object);
        }

        [Test]
        public void GetPurchase_NoParameters_ReturnSuccess()
        {
            var purchase = new List<PurchaseModel>
            {
                new PurchaseModel()
                {
                    PurchaseID= 6,
                    ProductID= 4,
                    ProductName= "Laptop",
                    Quantity= 4,
                    UnitPrice= 50000,
                    UnitDiscount= 10,
                    TotalDiscount= 24999,
                    TotalAmount= 180000,
                    PurchasedDate= null,
                    CreatedBy= null,
                    ModifiedBy= null,
                    CreatedDate= null,
                    ModifiedDate= null
                }
            };

            _dataAccess.Setup(x => x.GetPurchaseDetails()).ReturnsAsync(purchase);
            var result = _provider.GetPurchaseDetails().GetAwaiter().GetResult();

            Assert.That(result, Is.Not.Null);
            Assert.IsTrue(result.Count > 0);
        }

        [Test]
        public void sendProductIdQuantity_insertPurchaseDetail_ReturnSuccess()
        {
            _dataAccess.Setup(x => x.CreatePurchaseDetails(1, 10, "Vijay")).ReturnsAsync("Successfully purchased!");
            var result = _provider.CreatePurchaseDetails(1, 10, "Vijay").GetAwaiter().GetResult();

            Assert.AreEqual("Successfully purchased!", result);
        }

        [Test]
        public void DeletePurchse_SendPurchaseId_ReturnSuccess()
        {
            _dataAccess.Setup(x => x.DeletePurchaseDetailsById(1)).ReturnsAsync("Deleted Successfully!");
            var result = _provider.DeletePurchaseDetailsById(1).GetAwaiter().GetResult();

            Assert.AreEqual("Deleted Successfully!", result);
        }
    }
}
