﻿using System;

namespace GetFivePersonHeightAndFindChildOrAdult
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("What is your name:");
                    var name = Console.ReadLine();

                    Console.WriteLine("What is your height:");
                    var height = Convert.ToSingle(Console.ReadLine());

                    if (height >= 24 && height <= 40)
                    {
                        Console.WriteLine($"This person height is {height}. So this is consider as a child");
                    }
                    else if (height >= 60 && height <= 78)
                    {
                        Console.WriteLine($"This person height is {height}. So this is consider as a adult");
                    }
                    else { 
                        Console.WriteLine($"This person height is {height}. So this is consider as a other categrory");
                    }
                }

                //ConsiderAdultWhen60To78Inches
                Console.WriteLine("\nConsider Adult When 60 To 78 Inches:");
                Console.WriteLine("======================================");
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("What is your name:");
                    var name = Console.ReadLine();

                    Console.WriteLine("What is your height:");
                    var height = Convert.ToSingle(Console.ReadLine());

                    while (!BetweenRanges(60, 78, height))
                    {
                        Console.WriteLine("Invalid height for the adult the height between 60 to 78 inches:");
                        height = Convert.ToSingle(Console.ReadLine());
                    }

                    Console.WriteLine($"This person height is {height}. So this is consider as a adult");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("This issue returns: " + e.Message);
            }
        }
        
        public static bool BetweenRanges(float a, float b, float number)
        {
            return (a <= number && b >= number);
        }
    }
}
