﻿--VJ-154 - DDL - SQL Modelling - Grouping the Products into Two categories
-----------
CREATE TABLE Products (
	ProductId BIGINT IDENTITY (1,1) PRIMARY KEY  NOT NULL,
	ProductName VARCHAR(50) NOT NULL,
	ProduceCategoryName VARCHAR(100) NOT NULL
)

INSERT INTO Products(ProductName, ProduceCategoryName) 
VALUES	('Hard Disk', 'Computer Peripherals'),
		('Keyboard', 'Computer Peripherals'),
		('Moniter', 'Computer Peripherals'),
		('i3 processor', 'Computer Processors'),
		('i7 processor', 'Computer Processors'),
		('i5 processor', 'Computer Processors')

INSERT INTO Orders (OrderDate, CustomerId)
VALUES 	(DATEADD(day, 1, GetDate()), 125),
		(DATEADD(day, 1, GetDate()), 125),
		(DATEADD(day, 1, GetDate()), 126)

INSERT INTO OrderDetails (ProductName, Quantity, Price, OrderId)
VALUES 	('i3 processor', '1', '5000', 1058),
		('i5 processor', '1', '5000', 1059),
		('i7 processor', '1', '5000', 1060)

--VJ-155 - DML - Altering an existing table with backfilling the data
----------
ALTER TABLE OrderDetails 
ADD 
	ProductId BIGINT NULL,
	FOREIGN KEY (ProductId) REFERENCES Products (ProductId)


UPDATE OrderDetails SET ProductId=4 WHERE ProductName='i3 processor';  
UPDATE OrderDetails SET ProductId=5 WHERE ProductName='i5 processor';  
UPDATE OrderDetails SET ProductId=6 WHERE ProductName='i7 processor';  

--ALTER TABLE OrderDetails MODIFY ProductId BIGINT NOT NULL;
ALTER TABLE dbo.OrderDetails ALTER COLUMN ProductId BIGINT NOT NULL;

--VJ-156 - DDL - Add Audit Columns to the Customer, Orders and Order Details Tables
-----------
ALTER TABLE Customers 
ADD  
	CreatedDate DATETIME,
	ModifiedDate DATETIME,
	CreatedBy VARCHAR(50),
	ModifiedBy VARCHAR(50)

ALTER TABLE Orders 
ADD  
	CreatedDate DATETIME,
	ModifiedDate DATETIME,
	CreatedBy VARCHAR(50),
	ModifiedBy VARCHAR(50)

ALTER TABLE OrderDetails 
ADD  
	CreatedDate DATETIME,
	ModifiedDate DATETIME,
	CreatedBy VARCHAR(50),
	ModifiedBy VARCHAR(50)

