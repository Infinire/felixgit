﻿--VJ-147
CREATE TABLE CustomersAddress (
	CustomerAddressId BIGINT NOT NULL PRIMARY KEY IDENTITY(1000, 1)
	, Address1 VARCHAR(200) 
	, Address2 VARCHAR(200) 
	, Zipcode VARCHAR(5) 
	, State VARCHAR(50) 
	, City VARCHAR(50) 
	, AddressType  nvarchar (255) NOT NULL CHECK (AddressType IN('Shipping Address', 'Billing Address')) DEFAULT 'Shipping Address'
	, CustomerId BIGINT NOT NULL
	, CONSTRAINT FK_CustomerAddress_Customer FOREIGN KEY (CustomerId) REFERENCES Customers (CustomerId)
);

--VJ-148
ALTER TABLE Customers 
	ADD Gender BIT NOT NULL CHECK(Gender IN (0, 1)), 
		PhoneNumber NUMERIC(10, 0) NOT NULL, 
		Email VARCHAR(100), 
		DateOfBirth DATE NOT NULL

--VJ-149
CREATE FUNCTION AGE(@DateOfBirth AS DATETIME)

RETURNS VARCHAR(100)
AS
BEGIN
DECLARE @Years AS INT
DECLARE @BirthdayDate AS DATETIME
DECLARE @Age AS VARCHAR(100)

--Calculate difference in years
SET @Years = DATEDIFF(YY, @DateOfBirth, GETDATE())

--Add years to DateOfBirth
SET @BirthdayDate = DATEADD(YY, @Years, @DateOfBirth)

--Subtract a year if birthday is after today
SET @Age = @Years - CASE WHEN @BirthdayDate>GETDATE() THEN 1 ELSE 0
END

--Return the result
IF (@Age < 1)
	BEGIN
		SET @Age='Invalid Age'
	END

RETURN @AGE
END

--Sample Exceution Query
------------------------
SELECT dbo.AGE('1995-08-09')

--VJ-150
--------
CREATE VIEW CustomersView AS
	SELECT
	FirstName,
	LastName,
	DateOfBirth,
	CAST (DATEDIFF(DD, DateOfBirth, GETDATE()) / 365.25 AS INT) AS [Age],
	Email,
	Gender,
	PhoneNumber From Customers

--VJ-151
--------
SELECT C.FirstName, C.LastName, C.CustomerId, O.OrderId, OD.OrderDetailsId, OD.ProductName
FROM Customers C
LEFT JOIN Orders O ON C.CustomerId = O.CustomerId
LEFT JOIN OrderDetails OD ON O.OrderId = OD.OrderId
WHERE OD.ProductName != 'Hard Disk';


--VJ-152
--------
SELECT C.FirstName, C.LastName, MIN(O.OrderDate) AS OrderDate
FROM Customers C 
INNER JOIN Orders O ON C.CustomerId = O.customerId 
INNER JOIN OrderDetails OD ON O.OrderId = OD.OrderId
WHERE OD.ProductName= 'Mouse' 
GROUP BY C.FirstName, C.LastName

--VJ-153
--------
USE SQLTrainings;
GO
CREATE PROCEDURE CreateCustomers
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Address1 VARCHAR(100),
	@City VARCHAR(50),
	@State VARCHAR(50),
	@Zipcode NVARCHAR(5),
	@Gender VARCHAR(10),
	@DateOfBirth DATE,
	@Email VARCHAR(50),
	@PhoneNumber VARCHAR(10)
AS
	--if(@Gender = 'Male') 
	--	BEGIN 
	--		SET @Gender = 1 
	--	END
	--ELSE 
	--	BEGIN 
	--		SET @Gender = 0
	--	END

	INSERT INTO dbo.Customers(FirstName, LastName, Address1, City, State, Zipcode, Gender, DateOfBirth, Email, PhoneNumber) 
	VALUES (@FirstName, @LastName, @Address1, @City, @State, @Zipcode, CASE WHEN @Gender='Male' THEN 1 ELSE 0 END, @DateOfBirth, @Email, @PhoneNumber)
GO  

EXECUTE CreateCustomers 'Vijay', 'Felix', 'VPET', 'Ariyalur', 'Tamilnadu', '621805', 'Male', '1995-09-09', 'VijayFelixRajC', '9025837261';  

SELECT * FROM dbo.Customers;
