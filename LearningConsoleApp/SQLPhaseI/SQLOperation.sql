﻿CREATE TABLE Customers (
	CustomerId BIGINT IDENTITY(100, 1) NOT NULL,
	FirstName VARCHAR(50) NOT NULL,
	LastName VARCHAR(50) NOT NULL,
	Address1 VARCHAR(200),
	ZIPCode VARCHAR(5) ,
	City VARCHAR(50),
	State VARCHAR(50)
);
CREATE UNIQUE CLUSTERED INDEX PK_Customers_CustomerId ON Customers (CustomerId);

CREATE TABLE Orders (
	OrderId BIGINT IDENTITY(1001, 1) NOT NULL PRIMARY KEY,
	OrderDate DATETIME NOT NULL,
	CustomerId BIGINT,
    CONSTRAINT FK_Orders_Customers FOREIGN KEY (CustomerId) REFERENCES Customers(CustomerId)
);

CREATE TABLE OrderDetails (
	OrderDetailsId BIGINT IDENTITY(100001, 1) NOT NULL PRIMARY KEY ,
	ProductName VARCHAR(50) NOT NULL,
	Quantity INT NOT NULL,
	Price DECIMAL(12,3) NOT NULL,
	OrderId BIGINT NOT NULL,
	CONSTRAINT FK_OrderDetails_Orders FOREIGN KEY (OrderId) REFERENCES Orders(OrderId)
);

INSERT INTO Customers (FirstName, LastName, Address1, ZIPCode, City, State)
VALUES ('John', 'Smith', 'Address1', '65412', 'Chennai', 'TN'),
	   ('Paul', 'O’Brien', 'Address1', '65412', 'Chennai', 'TN'),
	   ('John', 'Kennady', 'Address1', '65412', 'Chennai', 'TN'),
	   ('Sai', 'Krishnan', 'Address1', '65412', 'Chennai', 'TN'),
	   ('Pete', 'Joe', 'Address1', '65412', 'Chennai', 'TN'),
	   ('Mukesh', 'Ambonny', 'Address1', '65412', 'Denver', 'CO'),
	   ('Peter', 'Paul', 'Address1', '65412', 'Denver', 'CO'),
	   ('John', 'Fix', 'Address1', '65412', 'Denver', 'CO'),
	   ('Sri', 'Ram', 'Address1', '65412', 'Denver', 'Colorado'),
	   ('Micheal', 'Reo', 'Address1', '65412', 'Denver', 'Colorado'),
	   ('Pete', 'Joe', 'Address1', '65412', 'Ariyalur', 'TN');
	   
INSERT INTO Orders (OrderDate, CustomerId)
VALUES 	(GetDate(), 103),
		(GetDate(), 104),
		(GetDate(), 105),
		(GetDate(), 106),
		(GetDate(), 107),
		(GetDate(), 108),
		(GetDate(), 109),
		(GetDate(), 110),
		(GetDate(), 111),
		(GetDate(), 112),
		(GetDate(), 113);
		
INSERT INTO Customers (FirstName, LastName, Address1, ZIPCode, City, State)
VALUES ('User', 'Name', 'Address1', '65412', 'Chennai', 'TN'),
		('User2', 'Name', 'Address1', '65412', 'Chennai', 'TN'),
		('User3', 'Name', 'Address1', '65412', 'Chennai', 'TN'),
		('User4', 'Name', 'Address1', '65412', 'Chennai', 'TN'),
		('User5', 'Name', 'Address1', '65412', 'Chennai', 'TN'),
		('User6', 'Name', 'Address1', '65412', 'Chennai', 'TN'),
		('User7', 'Name', 'Address1', '65412', 'Chennai', 'TN'),
		('User8', 'Name', 'Address1', '65412', 'Chennai', 'TN'),
		('User9', 'Name', 'Address1', '65412', 'Chennai', 'TN'),
		('User10', 'Name', 'Address1', '65412', 'Chennai', 'TN');
		
-- Insert additional records
INSERT INTO Orders (OrderDate, CustomerId)
VALUES 	(DATEADD(day, 1, GetDate()), 111),
		(DATEADD(day, 1, GetDate()), 112),
		(DATEADD(day, 1, GetDate()), 113),
		(DATEADD(day, 1, GetDate()), 114),
		(DATEADD(day, 1, GetDate()), 115),
		(DATEADD(day, 1, GetDate()), 116),
		(DATEADD(day, 1, GetDate()), 117),
		(DATEADD(day, 1, GetDate()), 118),
		(DATEADD(day, 1, GetDate()), 119),
		(DATEADD(day, 1, GetDate()), 120)

INSERT INTO OrderDetails (ProductName, Quantity, Price, OrderId)
VALUES 	('Hard Disk', '1', '1500', 1017),
		('Hard Disk', '1', '1500', 1018),
		('Hard Disk', '1', '1500', 1019),
		('Hard Disk', '1', '1500', 1020),
		('Hard Disk', '1', '1500', 1021),
		('Hard Disk', '1', '1500', 1022),
		('Hard Disk', '1', '1500', 1023),
		('Hard Disk', '1', '1500', 1024),
		('Hard Disk', '1', '1500', 1025),
		('Hard Disk', '1', '1500', 1026),
		('Keyboard and Monitor', '1', '15000', 1027),
		('Keyboard and Monitor', '1', '15000', 1029),
		('Keyboard and Monitor', '1', '15000', 1030),
		('Pendrive', '1', '1000', 1031),
		('Pendrive', '1', '1000', 1032),
		('Pendrive', '1', '1000', 1033),
		('Pendrive', '1', '1000', 1034),
		('Pendrive', '1', '1000', 1035),
		('Pendrive', '1', '1000', 1036),
		('Pendrive', '1', '1000', 1037),
		('Pendrive', '1', '1000', 1038),
		('Pendrive', '1', '1000', 1039),
		('Pendrive', '1', '1000', 1040),
		('Pendrive', '1', '1000', 1041),
		('Pendrive', '1', '1000', 1042),
		('Pendrive', '1', '1000', 1043),
		('Pendrive', '1', '1000', 1044),
		('Hard Disk', '1', '1000', 1045),
		('Hard Disk', '1', '1000', 1046);

--Find the date on which maximum number of orders created
--=======================================================
;WITH CTE (OrderDate) AS (
SELECT CAST(OrderDate AS DATE) FROM [SQLTrainings].[dbo].[Orders])
SELECT TOP 1 OrderDate, COUNT(orderdate) AS MaxOrderDate FROM CTE GROUP BY OrderDate order by MaxOrderDate desc

SELECT DISTINCT
    C.FirstName,
    C.LastName 
FROM Customers C
JOIN Orders O ON C.CustomerID = O.CustomerID
JOIN OrderDetails OD ON O.OrderId = OD.OrderId
WHERE OD.ProductName = 'Hard Disk'

SELECT TOP 1
	C.FirstName, 
	C.LastName, 
	SUM(OD.Price)
FROM Customers C
INNER JOIN Orders O ON C.CustomerId = O.CustomerId
INNER JOIN OrderDetails OD ON O.OrderId = OD.OrderId 
GROUP BY C.FirstName, C.LastName
ORDER BY SUM(OD.Price) DESC

SELECT DISTINCT
    C.FirstName,
    C.LastName 
FROM Customers C
INNER JOIN Orders O ON C.CustomerID = O.CustomerID
INNER JOIN OrderDetails OD ON O.OrderId = OD.OrderId
WHERE OD.ProductName != 'Hard Disk'

SELECT 
	C.FirstName, 
	C.LastName 
FROM Customers C
INNER JOIN Orders O ON C.CustomerId = O.CustomerId
INNER JOIN OrderDetails OD ON O.OrderId = OD.OrderId 
WHERE OD.ProductName = 'Keyboard and Monitor';

SELECT
	C.CustomerId,
	C.FirstName, 
	C.LastName,
	SUM(OD.Price)
FROM Customers C 
JOIN Orders O ON C.CustomerId = O.CustomerId
JOIN OrderDetails OD ON O.OrderId = OD.OrderId
GROUP BY C.FirstName, 
		C.LastName, 
		C.CustomerId
ORDER BY C.CustomerId

SELECT 
	FirstName, 
	LastName, 
	State 
From Customers 
WHERE State IN ('CO','Colorado');

SELECT * FROM Customers 
WHERE City = 'Denver';

SELECT * FROM Customers 
WHERE FirstName LIKE 'o’%' OR LastName LIKE 'o’%';

SELECT * FROM Customers 
ORDER BY CustomerId ASC

SELECT * FROM Customers 
ORDER BY TRIM(FirstName) ASC, 
		TRIM(LastName) DESC;

SELECT COUNT(*) FROM Customers;

SELECT COUNT(*) FROM Customers 
WHERE State IN ('CO', 'Colorado');

UPDATE Customers 
SET Address1 = 'Chennai' 
WHERE FirstName = 'Pete' AND LastName = 'Joe';

UPDATE Customers 
SET Address1 = 'Change Address' 
WHERE FirstName = 'John' AND LastName = 'Smith'

SELECT 
	C.FirstName, 
	C.LastName, 
	O.OrderId, 
	SUM(OD.Price)
FROM Customers C
INNER JOIN Orders O ON C.CustomerId = O.CustomerId
INNER JOIN OrderDetails OD ON O.OrderId = OD.OrderId 
GROUP BY O.OrderId, 
		C.FirstName, 
		C.LastName

SELECT TOP 1
	C.FirstName, 
	C.LastName, 
	SUM(OD.Price)
FROM Customers C
INNER JOIN Orders O ON C.CustomerId = O.CustomerId
INNER JOIN OrderDetails OD ON O.OrderId = OD.OrderId 
GROUP BY C.FirstName, 
		C.LastName
ORDER BY SUM(OD.Price) DESC

SELECT TOP 1
	C.FirstName, 
	C.LastName, 
	O.OrderId, 
	SUM(OD.Price) AS Price
FROM Customers C
INNER JOIN Orders O ON O.CustomerId = C.CustomerId
INNER JOIN OrderDetails OD ON O.OrderId = OD.OrderId
GROUP BY O.OrderId, 
		C.CustomerId, 
		C.FirstName, 
		C.LastName
ORDER BY SUM(OD.Price) DESC

SELECT
	C.FirstName, 
	C.LastName, 
	O.OrderId, 
	O.OrderDate,
	SUM(OD.Price) AS Price
FROM Customers C
INNER JOIN Orders O ON C.CustomerId = O.CustomerId
INNER JOIN OrderDetails OD ON O.OrderId = OD.OrderId
GROUP BY O.OrderId, 
		C.CustomerId, 
		C.FirstName, 
		C.LastName,  
		O.OrderDate
ORDER BY O.OrderDate DESC

SELECT TOP 1
	C.FirstName, 
	C.LastName, 
	O.OrderId, 
	O.OrderDate,
	SUM(OD.Price) AS Price
FROM Customers C
INNER JOIN Orders O ON C.CustomerId = O.CustomerId
INNER JOIN OrderDetails OD ON O.OrderId = OD.OrderId
GROUP BY O.OrderId, 
		C.CustomerId, 
		C.FirstName, 
		C.LastName,  
		O.OrderDate
ORDER BY O.OrderDate DESC

SELECT * FROM OrderDetails 
WHERE ProductName = 'KeyBoard';

SELECT 
		OrderId, 
		ProductName 
FROM  OrderDetails
GROUP BY OrderId, ProductName
HAVING (COUNT(OrderId) > 1)

SELECT 
		O.OrderId, 
		OD.ProductName,
		O.OrderDate
FROM Orders O 
JOIN Customers C ON C.CustomerId = O.CustomerId
JOIN OrderDetails OD ON OD.OrderId = O.OrderId
ORDER BY O.OrderDate DESC