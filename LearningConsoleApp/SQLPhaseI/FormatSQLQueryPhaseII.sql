﻿--a. select * from patient order by patientid
SELECT * FROM Patient 
ORDER BY PatientId

--b.select e.id,e.code,e.firstname,e.lastname,l.code,l.descr from employees e inner join location l on e.locationid=l.id
SELECT 
		E.EmployeeId
		, E.Code
		, E.FirstName
		, E.LastName
		, L.Code
		, L.Descr		
FROM Employees E 
INNER JOIN Location L ON E.LocationId = L.Id

--c. select employee.id,employee.first_name,employee.last_name,sum(datediff("second", call.start_time, call.end_time)) as call_duration_sum from call inner join employee on call.employee_id = employee.id group by employee.id, employee.first_name, employee.last_name order by employee.id asc;
SELECT 
		E.EmployeeId
		, E.FirstName
		, E.LastName
		, SUM(DATEDIFF("second", C.StartTime, C.EndTime)) AS CallDurationSum 
FROM Call C
INNER JOIN Employee E ON C.EmployeeId = E.EmployeeId 
GROUP BY E.EmployeeId
		, E.FirstName
		, E.LastName 
ORDER BY E.EmployeeId ASC;

--d. select employee.id,employee.first_name,employee.last_name,call.start_time, call.end_time,datediff("second", call.start_time, call.end_time) as call_duration,duration_sum.call_duration_sum,cast( cast(datediff("second", call.start_time, call.end_time) as decimal(7,2)) / cast(duration_sum.call_duration_sum as decimal(7,2)) as decimal(4,4)) as call_percentage from call inner join employee on call.employee_id = employee.id inner join (select employee.id,sum(datediff("second", call.start_time, call.end_time)) as call_duration_sum from call inner join employee on call.employee_id = employee.id group by employee.id) as duration_sum on employee.id = duration_sum.id order by employee.id asc,call.start_time asc;
SELECT 
		E.EmployeeId
		, E.FirstName
		, E.LastName
		, C.StartTime
		, C.EndTime
		, DATEDIFF("second", C.StartTime, C.EndTime) AS CallDuration
		, DS.CallDurationSum
		, CAST( CAST(DATEDIFF("second", C.StartTime, C.EndTime) AS DECIMAL(7,2)) / CAST(DS.CallDurationSum AS DECIMAL(7,2)) AS DECIMAL(4,4)) AS CallPercentage 
		
FROM Calls C
INNER JOIN Employees E ON C.EmployeeId = E.EmployeeId 
INNER JOIN (
			SELECT 
					E.EmployeeId
					, (DATEDIFF("second", C.StartTime, C.EndTime)) AS CallDurationSum 
			FROM Calls			
			INNER JOIN Employees ON C.EmployeeId = E.EmployeeId 
			GROUP BY E.EmployeeId) AS DurationSum ON E.EmployeeId = DS.DurationSumId 
			ORDER BY E.EmployeeId ASC
					, C.StartTime ASC;


--e. select [empid],[firstname] + ' ' +[lastname] as name  ,[education], [occupation],[yearlyincome], [sales]  from [employees 2015]  where yearlyincome >= 70000 union select [empid],[firstname] + ' ' +[lastname] as name  ,[education], [occupation],[yearlyincome], [sales]  from [employees 2016]  where yearlyincome < 70000  order by yearlyincome desc
SELECT 
		[EmpId]
		, CONCAT([FirstName], [LastName]) AS Name
		, [Education]
		, [Occupation]
		, [YearlyIncome]
		, [Sales]  
FROM [Employees2015]  
WHERE YearlyIncome >= 70000 
UNION 
	SELECT 
			[EmpId]
			, CONCAT([FirstName], [LastName]) AS Name
			, [Education]
			, [Occupation]
			, [YearlyIncome]
			, [Sales] 
	FROM [Employees2016]  
	WHERE YearlyIncome < 70000  
	ORDER BY YearlyIncome DESC

--f. create table tblcustomers(customerid bigint not null  primary key identity(1000,1),firstname varchar(50) not null,lastname varchar(50) not null,address1 varchar(200) ,zipcode varchar(5) ,city varchar(50) ,state varchar(50) );
CREATE TABLE TblCustomers (
	CustomerId BIGINT NOT NULL PRIMARY KEY IDENTITY(1000, 1)
	, FirstName VARCHAR(50) NOT NULL
	, LastName VARCHAR(50) NOT NULL
	, Address1 VARCHAR(200) 
	, Zipcode VARCHAR(5) 
	, City VARCHAR(50) 
	, State VARCHAR(50) 
);
