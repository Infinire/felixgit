﻿using System;

namespace ConsiderChildWhen24To40Inches
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("What is your name:");
                    var name = Console.ReadLine();

                    Console.WriteLine("What is your height:");
                    var height = Convert.ToSingle(Console.ReadLine());

                    if (height >= 24 && height <= 40)
                    {
                        Console.WriteLine($"This person height is {height}. So this is consider as a child");
                    }
                    else
                    {
                        Console.WriteLine($"This person is not between 24 to 40 inches. So not considering as child");
                    }

                }
            }
            catch (Exception e) {
                Console.WriteLine("This issue returns: " + e.Message) ;
            }
        }
    }
}
