﻿using System;
using System.Collections.Generic;

namespace FivePersonDetails
{
    public class ConsiderAdultWhen60To70Inches
    {
        public void Run()
        {
            try
            {
                for (int i = 0; i < 5; i++)

                {

                    Console.WriteLine("What is your name:");

                    var name = Console.ReadLine();


                    Console.WriteLine("What is your height:");

                    var height = Convert.ToSingle(Console.ReadLine());


                    while (!BetweenRanges(60, 78, height))

                    {

                        Console.WriteLine("Invalid height for the adult the height between 60 to 78 inches:");

                        height = Convert.ToSingle(Console.ReadLine());

                    }


                    Console.WriteLine($"This person height is {height}. So this is consider as a adult");

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("This issue returns: " + e.Message);
            }

            Console.ReadKey();
        }

        public static bool BetweenRanges(float a, float b, float number)

        {

            return (a <= number && b >= number);

        }
    }
}
