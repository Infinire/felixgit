﻿using System;
using System.Collections.Generic;

namespace FivePersonDetails
{
    public class getAnEmployeeSalaryBetween5000To10000
    {
        public void Run()
        {
            try
            {
                Console.WriteLine("What is your name");
                string name = Console.ReadLine();

                Console.WriteLine("What is your age");
                int age = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("What is your salary");
                decimal salary = Convert.ToDecimal(Console.ReadLine());

                while (!BetweenRanges(5000, 100000, salary))
                {
                    Console.WriteLine("Invalid salary entered. Salary range between 5000 to 100000.");
                    salary = (decimal)Convert.ToSingle(Console.ReadLine());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("This issue returns: " + e.Message);
            }

            Console.ReadKey();
        }

        public static bool BetweenRanges(int start, int end, decimal salary)
        {
            return (start <= salary && end >= salary);
        }
    }
}
