﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FivePersonDetails
{
    class getNameAgeSalaryOfFivePerson
    {
        public void Run()
        {
            try
            {
                List<Employee> persons = new List<Employee>();
                decimal totalAmountOfSalary = 0;
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("What is your name");
                    string name = Console.ReadLine();

                    Console.WriteLine("What is your age");
                    int age = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("What is your salary");
                    decimal salaryConversion = Convert.ToDecimal(Console.ReadLine());

                    var personDetails = new Employee { Name = name, Age = age, Salary = salaryConversion };

                    persons.Add(personDetails);
                }

                //a) Find the total amount of salary of all people.
                for (int j = 0; j < persons.Count; j++)
                {
                    totalAmountOfSalary += persons[j].Salary;
                }
                Console.WriteLine($"\nThe total amount of salaray of all people is: {totalAmountOfSalary}");

                //b) Find who is paid the highest and lowest salary.

                var getHighestSalaryPerson = GetHighestSalaryEmployee(persons);
                Console.WriteLine("\nThe highest employee salary from the list:");
                Console.WriteLine("---------------------------------");
                Console.WriteLine("Name \tAge \tSalary");
                Console.WriteLine("---------------------------------");
                Console.WriteLine($"{getHighestSalaryPerson.Name} \t{getHighestSalaryPerson.Age} \t{getHighestSalaryPerson.Salary}");

                var getLowestSalaryPerson = GetLowestSalaryEmployee(persons);
                Console.WriteLine("\nThe lowest employee salary from the list:");
                Console.WriteLine("---------------------------------");
                Console.WriteLine("Name \tAge \tSalary");
                Console.WriteLine("---------------------------------");
                Console.WriteLine($"{getLowestSalaryPerson.Name} \t{getLowestSalaryPerson.Age} \t{getLowestSalaryPerson.Salary}");

                //c) Find the average salary of all employees
                decimal averageOfAllEmployeeSalary = totalAmountOfSalary / persons.Count;
                Console.WriteLine($"\nAverage salary of all employees is: { String.Format("{0:0.##}", averageOfAllEmployeeSalary)}");

                //d) Find how many of them paid a salary which is greater than the average salary
                int aboveAverageCount = 0;
                for (int k = 0; k < persons.Count; k++)
                {
                    if (persons[k].Salary > averageOfAllEmployeeSalary)
                    {
                        aboveAverageCount++;
                    }
                }
                Console.WriteLine($"\nThe employee(s) are above average salary is: {aboveAverageCount}");

                //e) Find out whether the youngest or eldest person paid the highest salary.
                //If youngest is paid print youngest, if eldest is paid print eldest, if both print both
                var next = persons.Where(p=> !(p.Age == getHighestSalaryPerson.Age && p.Name == getHighestSalaryPerson.Name && p.Salary == getHighestSalaryPerson.Salary)).OrderByDescending(x=>x.Salary).FirstOrDefault();
                if (getHighestSalaryPerson.Age > next.Age) {
                    Console.WriteLine("\nThe eldest person getting highest salaray");
                }
                else if (next.Salary == getHighestSalaryPerson.Salary ) {
                    Console.WriteLine("The both are getting equal salary");
                } 
                else {
                    Console.WriteLine("\nThe youngest person getting highest salaray");
                }

                //f) Find the difference between highest and lowest salary
                decimal differenceForHighLowSalaryPerson = getHighestSalaryPerson.Salary - getLowestSalaryPerson.Salary;
                Console.WriteLine($"\nThe difference between highest and lowest salary is: {differenceForHighLowSalaryPerson}");

                //g) For each person print salary grade as follows
                List<Employee> aGrade = new List<Employee>();
                List<Employee> bGrade = new List<Employee>();
                List<Employee> cGrade = new List<Employee>();
                for (int b = 0; b < persons.Count; b++)
                {
                    if (BetweenRanges(0, 10000, persons[b].Salary))
                    {
                        cGrade.Add(persons[b]);
                    }
                    else if (BetweenRanges(10000, 50000, persons[b].Salary))
                    {
                        bGrade.Add(persons[b]);
                    }
                    else if (persons[b].Salary > 50000)
                    {
                        aGrade.Add(persons[b]);
                    }
                }

                //Print A grade
                Console.WriteLine("\nList of A Grade");
                Console.WriteLine("===========================");

                if (aGrade.Count > 0)
                {
                    Console.WriteLine("NAME \tAGE \tSALARY");
                    Console.WriteLine("----------------------");
                    for (int c = 0; c < aGrade.Count; c++)
                    {
                        Console.WriteLine($"{aGrade[c].Name} \t{aGrade[c].Age} \t{aGrade[c].Salary}");
                    }
                }
                else
                {
                    Console.WriteLine("There is no employee in A Grade\n");
                }

                //Print B grade
                Console.WriteLine("\nList of B Grade");
                Console.WriteLine("===========================");
                if (bGrade.Count > 0)
                {
                    Console.WriteLine("NAME \tAGE \tSALARY");
                    Console.WriteLine("----------------------");
                    for (int c = 0; c < bGrade.Count; c++)
                    {
                        Console.WriteLine($"{bGrade[c].Name} \t{bGrade[c].Age} \t{bGrade[c].Salary}");
                    }
                }
                else
                {
                    Console.WriteLine("There is no employee in B Grade\n");
                }

                //Print C grade
                Console.WriteLine("\nList of C Grade");
                Console.WriteLine("===========================");
                if (cGrade.Count > 0)
                {
                    Console.WriteLine("NAME \tAGE \tSALARY");
                    Console.WriteLine("----------------------");
                    for (int c = 0; c < cGrade.Count; c++)
                    {
                        Console.WriteLine($"{cGrade[c].Name} \t{cGrade[c].Age} \t{cGrade[c].Salary}");
                    }
                }
                else
                {
                    Console.WriteLine("There is no employee in C Grade\n");
                }

                //Number of employees in each Grade (a, b, c)
                Console.WriteLine($"\nThe number of employees in A grade is: {aGrade.Count}\n");
                Console.WriteLine($"The number of employees in B grade is: {bGrade.Count}\n");
                Console.WriteLine($"The number of employees in C grade is: {cGrade.Count}\n");


                //Find Average Age for A Grade
                if (aGrade.Count > 0)
                {
                    int aGradeTotalEmployeeAge = 0;
                    for (int d = 0; d < aGrade.Count; d++)
                    {
                        aGradeTotalEmployeeAge += aGrade[d].Age;
                    }
                    int averageOfAGrade = aGradeTotalEmployeeAge / aGrade.Count;
                    Console.WriteLine($"The average age for grade A employees is: {averageOfAGrade}\n");
                }
                else
                {
                    Console.WriteLine($"There is no employees in grade A\n");
                }

                //Find Average Age for B Grade
                if (bGrade.Count > 0)
                {
                    int bGradeTotalEmployeeAge = 0;
                    for (int e = 0; e < bGrade.Count; e++)
                    {
                        bGradeTotalEmployeeAge += bGrade[e].Age;
                    }
                    int averageOfBGrade = bGradeTotalEmployeeAge / bGrade.Count;
                    Console.WriteLine($"The average age for grade B employees is: {averageOfBGrade}\n");
                }
                else
                {
                    Console.WriteLine($"There is no employees in grade B\n");
                }

                //Find Average Age for C Grade
                if (cGrade.Count > 0)
                {
                    int cGradeTotalEmployeeAge = 0;
                    for (int f = 0; f < cGrade.Count; f++)
                    {
                        cGradeTotalEmployeeAge += cGrade[f].Age;
                    }
                    int averageOfCGrade = cGradeTotalEmployeeAge / cGrade.Count;
                    Console.WriteLine($"The average age for grade C employees is: {averageOfCGrade}\n");
                }
                else
                {
                    Console.WriteLine($"There is no employees in grade C\n");
                }

                //Order by salary of employees
                Console.WriteLine("Ascending the employess by salary wise");
                Console.WriteLine("======================================");
                var ascendingEmployeeDetailsBySalary = persons.OrderBy(employee => employee.Salary);
                Console.WriteLine($"NAME \tAGE \tSALARY");
                Console.WriteLine("---------------------------");
                foreach (var personDetail in ascendingEmployeeDetailsBySalary)
                {
                    Console.WriteLine($"{personDetail.Name} \t{personDetail.Age} \t{personDetail.Salary}");
                    Console.WriteLine("---------------------------");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Returns error message:", e.Message);
            }
        }

        public static Employee GetLowestSalaryEmployee(List<Employee> persons)
        {
            var person = new Employee();
            decimal minSalary = persons.Min(x => x.Salary);
            foreach (var e in persons.Where(e => e.Salary == minSalary))
                person = e;
            return person;
        }
        public static Employee GetHighestSalaryEmployee(List<Employee> persons)
        {
            var person = new Employee();
            for (int i = 0; i < persons.Count; i++)
            {
                if (persons[i].Salary > person.Salary)
                {
                    person.Name = persons[i].Name;
                    person.Age = persons[i].Age;
                    person.Salary = persons[i].Salary;
                }
            }
            return person;
        }
        
        public static bool BetweenRanges(int start, int end, decimal salary)
        {
            return (start <= salary && end >= salary);
        }
    }

    public class Employee
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public decimal Salary { get; set; }
    }
}