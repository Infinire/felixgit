﻿using System;
using System.Collections.Generic;

namespace FivePersonDetails
{
    public class GetPersonSalaryWithDecimalPlaces
    {
        public void Run()
        {
            List<Person> person = new List<Person>();

            Console.WriteLine("Please enter how many person details you need?");
            int numberOfPersonDetails = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < numberOfPersonDetails; i++)
            {
                Console.WriteLine("What is your name");
                string name = Console.ReadLine();

                Console.WriteLine("What is your age");
                int age = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("What is your salary");

                decimal salaryConversion = Convert.ToDecimal(Console.ReadLine());

                var personDetails = new Person { Name = name, Age = age, Salaray = salaryConversion };

                person.Add(personDetails);
            }

            Console.WriteLine("\nPERSON DETAILS");
            Console.WriteLine("===========================");
            Console.WriteLine("NAME \tAGE \tSALARY");
            Console.WriteLine("---------------------------\n");
            for (int j = 0; j < person.Count; j++)
            {
                Console.WriteLine($"{person[j].Name} \t{person[j].Age} \t{ String.Format("{0:0.##}", person[j].Salaray) }");
            }
            Console.ReadKey();
        }

        public class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public decimal Salaray { get; set; }
        }
        
    }
}
