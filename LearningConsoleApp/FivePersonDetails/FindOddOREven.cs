﻿using System;
using System.Collections.Generic;

namespace FivePersonDetails
{
    public class FindOddOrEven
    {
        public void Run()
        {
            try
            {
                for (int i = 0; i < 4; i++)
                {
                    Console.WriteLine("What is your name:");
                    var name = Console.ReadLine();

                    Console.WriteLine("What is your height:");
                    var height = Convert.ToSingle(Console.ReadLine());

                    if (height % 2 == 0)
                    {
                        Console.WriteLine("Entered a number is an Even Number");
                    }
                    else
                    {
                        Console.WriteLine("Entered a number is an Odd Number");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("This issue returns: " + e.Message);
            }

            Console.ReadKey();
        }
    }
}
