﻿using System;
using System.Collections.Generic;

namespace FivePersonDetails
{
    public class AskAQuestionToAPersonAboutTheirAge
    {
        public void Run()
        {
            try
            {
                Console.WriteLine("What is your age:");
                var age = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("The person age is: " + age);

                if (age > 50)
                {
                    Console.WriteLine("Do you exercise daily?");
                    var exercise = Console.ReadLine();

                    if (exercise.ToLower() == "yes" || exercise.ToLower() == "y")
                    {

                        Console.WriteLine("How many hours do you have to exercise?");
                        double hours = Convert.ToDouble(Console.ReadLine());

                        if (hours > 1)
                        {
                            Console.WriteLine("You are doing great!");
                        }
                        else
                        {
                            Console.WriteLine("It is good to keep up warming.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Do you have any health conditions?");
                        var healthCondition = Console.ReadLine();
                        if (healthCondition.ToLower() == "yes" || healthCondition.ToLower() == "y")
                        {
                            Console.WriteLine("You should exercise daily!");
                        }
                        else
                        {
                            Console.WriteLine("It is good that your are still healthy!");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("How often do you go for a movie?");
                    var goForMovie = Console.ReadLine();

                    if (goForMovie.ToLower() == "once a month" || goForMovie.ToLower() == "once in a month")
                    {
                        Console.WriteLine("What kind of movie do you watch?");
                        var whatKindOfMovie = Console.ReadLine();
                        if (whatKindOfMovie.ToLower() == "all genre")
                        {
                            Console.WriteLine("It is good entertainment!");
                        }
                        else
                        {
                            Console.WriteLine("Good to watch those movies");
                        }
                    }
                    else if ((goForMovie.ToLower() == "i do not watch a movie") || (goForMovie.ToLower() == "i don't watch a movie"))
                    {
                        Console.WriteLine("Do you read books? ");
                        var doYouReadBooks = Console.ReadLine();
                        if (doYouReadBooks.ToLower() == "yes")
                        {
                            Console.WriteLine("It is good to have time passed");
                        }
                        else
                        {
                            Console.WriteLine("Probably you are a cricket player.");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("This issue returns: " + e.Message);
            }

            Console.ReadKey();
        }
    }
}
