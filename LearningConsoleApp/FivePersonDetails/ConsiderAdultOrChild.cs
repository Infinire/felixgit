﻿using System;
using System.Collections.Generic;

namespace FivePersonDetails
{
    public class ConsiderAdultOrChild
    {
        public void Run()
        {
            try
            {
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("What is your name:");
                    var name = Console.ReadLine();

                    Console.WriteLine("What is your height:");
                    var height = Convert.ToSingle(Console.ReadLine());

                    if (height >= 24 && height <= 40)
                    {
                        Console.WriteLine($"This person height is {height}. So this is consider as a child");
                    }
                    else if (height >= 60 && height <= 78)
                    {
                        Console.WriteLine($"This person height is {height}. So this is consider as a adult");
                    }
                    else
                    {
                        Console.WriteLine($"This person height is {height}. So this is consider as a other categrory");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("This issue returns: " + e.Message);
            }

            Console.ReadKey();
        }
    }
}
