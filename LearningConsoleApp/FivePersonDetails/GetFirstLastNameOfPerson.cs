﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FivePersonDetails
{
    class GetFirstLastNameOfAPerson
    {
        public void Run()
        {
            try
            {
                Console.WriteLine("Enter first name");
                var firstName = Console.ReadLine();

                while (firstName.ToLower().Contains('z') || firstName.ToLower().Contains("'"))
                {
                    Console.WriteLine("Invalid character' or 'z' entered");
                    firstName = Console.ReadLine();
                }

                Console.WriteLine("Enter last name");
                var lastName = Console.ReadLine();

                //a) How many characters in first & last name
                Console.WriteLine($"Number of character in first name: {firstName.Length}");
                Console.WriteLine($"Number of character in last name: {lastName.Length}");

                //b) Find if the first name is equal to last name
                if (firstName.ToLower() == lastName.ToLower())
                {
                    Console.WriteLine("The first name is equal to last name");
                }
                else
                {
                    Console.WriteLine("The first & last names are not equal");
                }

                //c) Find if the first name is equal to John
                if (firstName.ToLower() == "john")
                {
                    Console.WriteLine("The first is equal to John");
                }
                else
                {
                    Console.WriteLine("The first name is not equal to John");
                }

                //d) Find if the last name contains English vowels 
                var isVowelExist = FindVowelsInString(lastName);
                Console.WriteLine($"The last name constain {isVowelExist} English vowels");

                //e) Find the first letter of first name and last name
                Console.WriteLine($"The first name of first character is: {firstName[0].ToString()}");
                Console.WriteLine($"The last name of first character is: {lastName[0].ToString()}");

                //f) Print the middle letter of a first name. If the middle letter is two letters, print both letters.
                var offset = firstName.Length % 2 == 0 ? 1 : 0;
                var middle = firstName.Substring(firstName.Length / 2 - offset, offset + 1);

                Console.WriteLine($"The middle char of firstName : { middle }");

                //g) Print the first name in upper case and last name in lower case.
                Console.WriteLine($"The first name in uppercase & lastname in lowercase: {firstName.ToUpper() + ' ' + lastName.ToLower()}");

            }
            catch (Exception e)
            {
                Console.WriteLine("An issue occur: ", e.Message);
            }
        }

        private static int FindVowelsInString(string inputString)
        {
            if (inputString == null) return 0;
            var vowels = new List<char>() { 'a', 'e', 'i', 'o', 'u' };
            var result = inputString.ToLower().Count(c => vowels.Contains(c));
            return result;
        }
    }
}
