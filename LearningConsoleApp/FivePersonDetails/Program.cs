﻿using System;

namespace FivePersonDetails
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("=====================================");
            Console.WriteLine("1 - FivePersonDetails");
            Console.WriteLine("2 - ConsiderAdult60To70Inches - VJ-126");
            Console.WriteLine("3 - ConsiderAdultOrChildBasedHeight - VJ-127");
            Console.WriteLine("4 - FindOddOrEvenDetails VJ-128");
            Console.WriteLine("5 - GetPersonSalaryWithDecimalPlaces VJ-129");
            Console.WriteLine("6 - GetNameAgeSalaryOfFiveEmployee VJ-130");
            Console.WriteLine("7 - GetNameAgeSalaryOfAnEmployee VJ-131");
            Console.WriteLine("8 - GetFirstLastNameOfPerson VJ-132");
            Console.WriteLine("9 - CreateASentenceIHaveBoughtACar VJ-133");
            Console.WriteLine("10 - AskAQuestionWhatisyourAge VJ-134, VJ-135, VJ-136");

            Console.WriteLine("=====================================");
            Console.Write("Choose Option below to run the cases:");

            var key = Console.ReadLine();
            switch (key)
            {
                case "1":
                {
                    var fivePerson = new FivePersonData();
                    fivePerson.Run();
                    break;
                }
                case "2":
                {
                    var considerAdultWhen60To70Inches = new ConsiderAdultWhen60To70Inches();
                    considerAdultWhen60To70Inches.Run();
                    break;
                }
                case "3":
                {
                    var considerAdultOrChild = new ConsiderAdultOrChild();
                        considerAdultOrChild.Run();
                    break;
                }
                case "4":
                {
                    var oddOrEven = new FindOddOrEven();
                    oddOrEven.Run();
                    break;
                }
                case "5":
                {
                    var personSalaryWithDecimalPlace = new GetPersonSalaryWithDecimalPlaces();
                    personSalaryWithDecimalPlace.Run();
                    break;
                }
                case "6":
                {
                    var getNameAgeSalaryOfFivePerson = new getNameAgeSalaryOfFivePerson();
                        getNameAgeSalaryOfFivePerson.Run();
                    break;
                }
                case "7":
                {
                    var getAnEmployeeSalaryBetween5000To10000 = new getAnEmployeeSalaryBetween5000To10000();
                    getAnEmployeeSalaryBetween5000To10000.Run();
                    break;
                }
                case "8":
                {
                    var getFirstLastNameOfAPerson = new GetFirstLastNameOfAPerson();
                        getFirstLastNameOfAPerson.Run();
                    break;
                }
                case "9":
                {
                    var boughtANewCar = new CreateSentenceBoughtNewCar();
                    boughtANewCar.Run();
                    break;
                }
                case "10":
                {
                    var askQuestionWhatisyourAge = new AskAQuestionToAPersonAboutTheirAge();
                    askQuestionWhatisyourAge.Run();
                    break;
                }
            }
        }
    }
}
