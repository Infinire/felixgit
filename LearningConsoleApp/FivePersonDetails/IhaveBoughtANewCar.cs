﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace FivePersonDetails
{
    public class CreateSentenceBoughtNewCar
    {
        private const string V = ".";
        public void Run()
        {
            try
            {
                //a) Print if the sentence contains full stop.
                Console.WriteLine("Enter a Sentence");
                string strWord = Console.ReadLine();

                Console.WriteLine($"Enter a Sentence {strWord.Substring(strWord.Length - 1)}");
                if (strWord.Substring(strWord.Length - 1) == V)
                {
                    Console.WriteLine($"Your sentence is: {strWord}");
                }
                else
                {
                    Console.WriteLine("Your sentence should not contains full stop.");
                }

                //b) Find how many letters are English vowels.
                var isVowelExist = CountVowelsInAString(strWord);
                Console.WriteLine($"The sentence constains {isVowelExist} English vowels");

                //c) Find how many words in this sentence.
                string[] word = strWord.Split(' ');
                Console.WriteLine("The number of words in this sentence is: " + word.Length);

                //d) Find if it contains a letter z
                Regex re = new Regex("['zZ]*$");
                if (re.IsMatch(strWord))
                {
                    Console.WriteLine("The sentence contains 'z' ");
                }
                else
                {
                    Console.WriteLine("Not Match");
                }

                //e) Find if a sentence contains word car
                bool carWordExist = word.Contains("car");
                Console.WriteLine("The sentence contains car word: " + carWordExist);

                //f) Find in which position word new start with
                int indexOfString = strWord.IndexOf("new");
                Console.WriteLine("The position of new start with" + indexOfString + "th position");


                //g) Find if it contains any verb in past or present tense
                Console.WriteLine("Find if it contains any verb in past or present tense:");
                Console.WriteLine("------------------------------------------------------");
                string[] allVerbs = new[] { "eat", "drink", "have", "had", "has", "am", "are", "bought" }; // etc
                //string s = "I have bought a new car";
                var words = strWord.Split(' ');
                List<string> verbList = new List<string>();
                foreach (var item in words)
                {
                    if (allVerbs.Contains(item.ToLower()))
                        verbList.Add(item);
                }

                foreach (var list in verbList)
                {
                    Console.WriteLine("The verbs in a sentence is: " + list);
                }

                //h) Replace the word car with house.
                var strChange = strWord.Replace("car", "house");
                Console.WriteLine("\nThe car word replaced with the hourse : " + strChange);

                //i) Create an array of words as house, home, watch, bike, mobile phone and shirt. Replace the word car with each of the words in the array.
                Console.WriteLine("\nReplace the word car with other words in array list:");
                Console.WriteLine("----------------------------------------------------");
                string[] strArray = new string[] { "house", "home", "watch", "bike", "mobile", "phone", "shirt" };
                foreach (string item in strArray)
                {
                    Console.WriteLine("The sentence change: " + strWord.Replace("car", item));
                }

                //j) Change the sentence to I have bought an old car.
                Console.WriteLine("\nChange the sentence from 'a new' to 'an old'");
                Console.WriteLine("---------------------");
                Console.WriteLine(strWord.Replace("a new", "an old"));

                //k) Merge all words together without space
                Console.WriteLine("\nMerge all words together without space");
                Console.WriteLine("---------------------");
                Console.WriteLine(strWord.Replace(" ", ""));

                //l) Remove the word have from the sentence
                Console.WriteLine("\nRemove the word have from the sentence");
                Console.WriteLine("---------------------");
                Console.WriteLine(strWord.Replace("have ", ""));

            }
            catch (Exception e)
            {
                Console.WriteLine("This issue returns: " + e.Message);
            }

            Console.ReadKey();
        }
        private static int CountVowelsInAString(string inputString)
        {
            if (inputString == null) return 0;
            var vowels = new List<char>() { 'a', 'e', 'i', 'o', 'u' };
            var result = inputString.ToLower().Count(c => vowels.Contains(c));
            return result;
        }
    }
}
