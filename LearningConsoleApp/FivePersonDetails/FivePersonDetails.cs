﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace FivePersonDetails
{
    public class FivePersonData
    {
        public void Run()
        {
            List<Person> persons = new List<Person>();

            for (var i = 0; i < 5; i++)
            {
                Console.WriteLine("What is your name:\n");
                var name = Console.ReadLine();

                Console.WriteLine("What is your height:\n");
                var height = Convert.ToSingle(Console.ReadLine());

                var detail = new Person { Name = name, Height = height };

                persons.Add(detail);
            }

            foreach (var value in persons)
            {
                //Console.WriteLine($"Result: { value.Name } : { value.Height }"); //one way of printing the value
                Console.WriteLine("Result: "+ value.Name +":"+ value.Height); // Anoter way of printing the value
            }

            //Get the tallest person
            var getTallestPerson = GetHeighestPerson(persons);
            Console.WriteLine("\nThe Tallest person from the list = " + getTallestPerson.Name + ":" + getTallestPerson.Height);

            //Get the shortest person
            var getShortestPerson = GetShortestPerson(persons);
            Console.WriteLine("\nThe shortest person from the list = " + getShortestPerson.Name + ":" + getShortestPerson.Height);

            //Get Average Hight
            var getAverageHeight = GetAverageHeight(persons);
            Console.WriteLine("\nThe average height is = " + getAverageHeight);

            //Find the second largest person
            var secondLargestPerson = GetSecondLargestPerson(persons);
            Console.WriteLine("\nThe second largest person is = " + secondLargestPerson.Name + ":" + secondLargestPerson.Height);

            //Find the closest persons near average
            var closestPerson = GetClosestPersonFromAverage(persons, getAverageHeight);
            Console.WriteLine("\nThe closest person is = " + secondLargestPerson.Name + ":" + secondLargestPerson.Height);

            //Find the closest persons near average
            var findAboveAndBelowPersonFromAverageHeight = GetAboveAndBelowPersonsFromAverage(persons, getAverageHeight);
            Console.WriteLine($"\nThe numbers below the average are: { findAboveAndBelowPersonFromAverageHeight.Item1}");
            Console.WriteLine($"\nThe numbers above the average are: {findAboveAndBelowPersonFromAverageHeight.Item2}");

            //Find the difference of height between tallest and shortest person
            var findDiffereceBetweenTallestAndShortest = GetDiffereceBetweenTallandShortestPerson(getTallestPerson.Height, getShortestPerson.Height);
            Console.WriteLine($"\nThe difference between tallest and shortest person is: {findDiffereceBetweenTallestAndShortest}");

            Console.ReadKey();
        }
        
        //Find the difference between tallest and shortest person
        public static float GetDiffereceBetweenTallandShortestPerson(float tall, float shortest) {
            float difference = 0;
            difference = tall - shortest;
            return difference;
        }

        //Get the closest value
        public static Tuple<int, int> GetAboveAndBelowPersonsFromAverage(List<Person> persons, float averageNumber)
        {
            int below = 0, above = 0;

            for (int i = 0; i < persons.Count; i++) {
                if (averageNumber > persons[i].Height)
                {
                    below++;
                }
                else { above++; }
            }
            return new Tuple<int, int>(below, above);
        }

        //Get the closest value
        public static Person GetClosestPersonFromAverage(List<Person> persons, float averageNumber) {
            var closest = persons.OrderBy(v => Math.Abs((long)v.Height - averageNumber)).First();
            return closest;
        }

        //Get the Second Largest person
        public static Person GetSecondLargestPerson(List<Person> persons)
        {
            var person = new Person();

            if (persons.Count < 2) {
                Console.WriteLine("\n Invalid Input ");
            }
            var personDetailsByOrders = persons.OrderByDescending(h => h.Height);
            //var personDetailsByOrders = from p in persons orderby p.Height descending select p; //another way of sorting by "linkq"

            var i = 0;
            foreach (var personDetail in personDetailsByOrders) { 
                person.Name = personDetail.Name;
                person.Height = personDetail.Height;

                if (i == 1) //check for the second name & height and break the loop
                    break;
                i++;
            }
            return person;
        }

        //Get Average Height
        public static float GetAverageHeight(List<Person> persons)
        {
            float sum = 0;
            float average = 0.0f;
            for (int i = 0; i < persons.Count; i++)
            {
                sum += persons[i].Height;
            }
            average = sum / persons.Count;
            return average;
        }

        public static Person GetHeighestPerson(List<Person> persons)
        {
            var person = new Person();
            for (int i = 0; i < persons.Count; i++)
            {
                if (persons[i].Height > person.Height)
                {
                    person.Name = persons[i].Name;
                    person.Height = persons[i].Height;
                }
            }
            return person;
        }

        public static Person GetShortestPerson(List<Person> persons)
        {
            var person = persons[0];
            for (int i = 0; i < persons.Count; i++)
            {
                if (persons[i].Height < person.Height)
                {
                    person.Name = persons[i].Name;
                    person.Height = persons[i].Height;
                }
            }
            return person;
        }

    }

    public class Person
    {
        public string Name { get; set; }
        public float Height { get; set; }
    
    }

}
