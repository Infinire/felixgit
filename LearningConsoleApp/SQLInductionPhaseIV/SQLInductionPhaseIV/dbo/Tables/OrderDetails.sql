﻿CREATE TABLE [dbo].[OrderDetails] (
    [OrderDetailsId] BIGINT          IDENTITY (100001, 1) NOT NULL,
    [ProductName]    VARCHAR (50)    NOT NULL,
    [Quantity]       INT             NOT NULL,
    [Price]          DECIMAL (12, 3) NOT NULL,
    [OrderId]        BIGINT          NOT NULL,
    [ProductId]      BIGINT          NULL,
    PRIMARY KEY CLUSTERED ([OrderDetailsId] ASC),
    FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Products] ([ProductId]),
    CONSTRAINT [FK_OrderDetails_Orders] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Orders] ([OrderId])
);

