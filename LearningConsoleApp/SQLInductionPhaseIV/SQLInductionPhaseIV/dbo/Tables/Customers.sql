﻿CREATE TABLE [dbo].[Customers] (
    [CustomerId]   BIGINT        IDENTITY (100, 1) NOT NULL,
    [FirstName]    VARCHAR (50)  NOT NULL,
    [LastName]     VARCHAR (50)  NOT NULL,
    [Address1]     VARCHAR (200) NULL,
    [ZIPCode]      VARCHAR (5)   NULL,
    [City]         VARCHAR (50)  NULL,
    [State]        VARCHAR (50)  NULL,
    [Gender]       BIT           NOT NULL,
    [PhoneNumber]  NUMERIC (10)  NOT NULL,
    [Email]        VARCHAR (100) NULL,
    [DateOfBirth]  DATE          NOT NULL,
    [CreatedDate]  DATETIME      NULL,
    [ModifiedDate] DATETIME      NULL,
    [CreatedBy]    VARCHAR (50)  NULL,
    [ModifiedBy]   VARCHAR (50)  NULL,
    CHECK ([Gender]=(1) OR [Gender]=(0))
);


GO
CREATE UNIQUE CLUSTERED INDEX [PK_Customers_CustomerId]
    ON [dbo].[Customers]([CustomerId] ASC);

