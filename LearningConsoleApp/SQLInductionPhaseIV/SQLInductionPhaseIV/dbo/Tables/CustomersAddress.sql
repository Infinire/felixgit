﻿CREATE TABLE [dbo].[CustomersAddress] (
    [CustomerAddressId] BIGINT         IDENTITY (1000, 1) NOT NULL,
    [Address1]          VARCHAR (200)  NULL,
    [Address2]          VARCHAR (200)  NULL,
    [Zipcode]           VARCHAR (5)    NULL,
    [State]             VARCHAR (50)   NULL,
    [City]              VARCHAR (50)   NULL,
    [AddressType]       NVARCHAR (255) DEFAULT ('Shipping Address') NOT NULL,
    [CustomerId]        BIGINT         NOT NULL,
    PRIMARY KEY CLUSTERED ([CustomerAddressId] ASC),
    CHECK ([AddressType]='Billing Address' OR [AddressType]='Shipping Address'),
    CONSTRAINT [FK_CustomerAddress_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

