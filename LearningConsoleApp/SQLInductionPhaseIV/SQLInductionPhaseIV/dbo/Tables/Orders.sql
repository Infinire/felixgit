﻿CREATE TABLE [dbo].[Orders] (
    [OrderId]      BIGINT       IDENTITY (1001, 1) NOT NULL,
    [OrderDate]    DATETIME     NOT NULL,
    [CustomerId]   BIGINT       NULL,
    [CreatedDate]  DATETIME     NULL,
    [ModifiedDate] DATETIME     NULL,
    [CreatedBy]    VARCHAR (50) NULL,
    [ModifiedBy]   VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([OrderId] ASC),
    CONSTRAINT [FK_Orders_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

