﻿CREATE TABLE [dbo].[Products] (
    [ProductId]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [ProductName]         VARCHAR (50)  NOT NULL,
    [ProduceCategoryName] VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([ProductId] ASC)
);

