﻿CREATE PROCEDURE CreateCustomers
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Address1 VARCHAR(100),
	@City VARCHAR(50),
	@State VARCHAR(50),
	@Zipcode NVARCHAR(5),
	@Gender VARCHAR(10),
	@DateOfBirth DATE,
	@Email VARCHAR(50),
	@PhoneNumber VARCHAR(10)
AS
	--if(@Gender = 'Male') 
	--	BEGIN 
	--		SET @Gender = 1 
	--	END
	--ELSE 
	--	BEGIN 
	--		SET @Gender = 0
	--	END


	INSERT INTO dbo.Customers(FirstName, LastName, Address1, City, State, Zipcode, Gender, DateOfBirth, Email, PhoneNumber) 
	VALUES (@FirstName, @LastName, @Address1, @City, @State, @Zipcode, CASE WHEN @Gender='Male' THEN 1 ELSE 0 END, @DateOfBirth, @Email, @PhoneNumber)
