﻿CREATE PROCEDURE GetCustomersEvenSigleIfOrdered
AS
SELECT O.OrderId, 
		C.FirstName + ' ' + C.LastName, 
		OD.ProductName
FROM Orders O
JOIN Customers C ON O.CustomerId = C.CustomerId
JOIN OrderDetails OD ON O.OrderId = OD.OrderId;