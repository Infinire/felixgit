﻿CREATE PROCEDURE [dbo].[CustomersProcedure]
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Address1 VARCHAR(100),
	@City VARCHAR(50),
	@State VARCHAR(50),
	@Zipcode NVARCHAR(5),
	@Gender VARCHAR(10),
	@DateOfBirth DATE,
	@Email NVARCHAR(50),
	@PhoneNumber NVARCHAR(10)
AS
	--if(@Gender = 'Male') 
	--	BEGIN 
	--		SET @Gender = 1 
	--	END
	--ELSE 
	--	BEGIN 
	--		SET @Gender = 0
	--	END


	INSERT INTO CustomersProcedure(FirstName,LastName, Address1, City, State, Zipcode, Gender, DateOfBirth, Email, PhoneNumber) 
	VALUES (@FirstName, @LastName, @Address1, @City, @State, @Zipcode, CASE WHEN @Gender='Male' THEN 1 ELSE 0 END, @DateOfBirth, @Email, @PhoneNumber)


EXECUTE CustomersProcedure @FirstName = N'Vijay', @LastName = N'Felix', @Address1 = N'VPET', @City = N'Ariyalur', 
@State = N'Tamilnadu', @Zipcode = N'621805', @Gender = N'Male', @DateOfBirth = N'1995-09-09', @Email=N'VijayFelixRajC', @PhoneNumber=N'9025837261';  
