﻿using System;
using System.Collections.Generic;

namespace ChildrenAdultNameHeightDetail
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> children = new List<Person>();
            List<Person> adult = new List<Person>();

            //Check for the count of each children & adult
            Console.WriteLine("Please add five children details:");
            Console.WriteLine("------------------------------------ \n");

            //Children operation
            for (int i=0; i < 5; i++) { 
                Console.WriteLine("What is your name:");
                var name = Console.ReadLine();

                Console.WriteLine("What is your height:");
                var height = Convert.ToSingle(Console.ReadLine());
                
                while (!BetweenRanges(24, 40, height)) { 
                    Console.WriteLine("Invalid height for the children the height between 24 to 40 inches:");
                    height = Convert.ToSingle(Console.ReadLine());
                }

                var childrenDetails = new Person { Name = name, Height = height };

                children.Add(childrenDetails);
            }

            Console.WriteLine("\nPlease add five adult details:");
            Console.WriteLine("--------------------------------\n");
            //Adult operation
            for (int j = 0; j < 5; j++)
            {
                Console.WriteLine("What is your name:");
                var adultName = Console.ReadLine();

                Console.WriteLine("What is your height:");
                var adultHeight = Convert.ToSingle(Console.ReadLine());
                while (!BetweenRanges(60, 78, adultHeight))
                {
                    Console.WriteLine("Invalid height for the adult the height between 60 to 78 inches:");
                    adultHeight = Convert.ToSingle(Console.ReadLine());
                }

                var adultDetails = new Person { Name = adultName, Height = adultHeight };

                adult.Add(adultDetails);
            }

            float childrenTotalHeight = 0;
            for (int i = 0; i < children.Count; i++) {
                childrenTotalHeight += children[i].Height;
            }

            //Average for Children
            float childrenAverageHeight = childrenTotalHeight / 5;
            Console.WriteLine($"The children average height is: {childrenAverageHeight}");

            float adultTotalHeight = 0;
            for (int i = 0; i < adult.Count; i++)
            {
                adultTotalHeight += adult[i].Height;
            }

            //Average for adult
            float adultAverageHeight = adultTotalHeight / 5;
            Console.WriteLine($"The adult average height is: { adultAverageHeight }");

            //Find the difference between adult and children
            float averageHeightForChildrenAdult = adultAverageHeight - childrenAverageHeight;
            Console.WriteLine($"The difference between child and adult: { averageHeightForChildrenAdult }");

            Console.ReadLine();
        }

        public static bool BetweenRanges(float a, float b, float number) {
            return (a <= number && b >= number);
        }
    }

    public class Person
    {
        public string Name;
        public float Height;
    }
}