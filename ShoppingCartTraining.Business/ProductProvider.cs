﻿using ShoppingCartTraining.Common.Models;
using ShoppingCartTraining.DataAccess;
namespace ShoppingCartTraining.Business
{
    public interface IProductProvider
    {
        Task<List<ProductModel>> GetProducts();
        Task<List<ProductModel>> GetProductById(int productId);
        Task<List<ProductModel>> CreateProduct(ProductModel data);
        Task<List<ProductModel>> UpdateProduct(ProductModel data);
        Task<bool> DeleteProduct(int productId);
    }
    public class ProductProvider: IProductProvider
    {
        private readonly IProductDataAccess _dataAccess;
        public ProductProvider(IProductDataAccess dataAccess)
        { 
            _dataAccess = dataAccess;    
        }
        public async Task<List<ProductModel>> GetProducts()
        {
            return await _dataAccess.GetProducts();
        }
        public async Task<List<ProductModel>> GetProductById(int productId)
        {
            return await _dataAccess.GetProductById(productId);
        }
        public async Task<List<ProductModel>> CreateProduct(ProductModel data) 
        {
            return await _dataAccess.CreateProduct(data);
        }
        public async Task<List<ProductModel>> UpdateProduct(ProductModel data)
        {
            return await _dataAccess.UpdateProduct(data);
        }
        public async Task<bool> DeleteProduct(int productId)
        {
            return await _dataAccess.DeleteProduct(productId);
        }
    }
}
