﻿using ShoppingCartTraining.Common.Models;
using ShoppingCartTraining.DataAccess;
namespace ShoppingCartTraining.Business
{
    public interface IReturnProvider
    {
        Task<List<ReturnViewModel>> ReturnDetails();
        Task<string> ReturnProducts(int purchaseID, int productID, int returnedQuantity);
        Task<string> DeleteReturnItemsByPurchaseID(int id);
        Task<List<PurchaseDetailsModel>> GetPurchaseDetails();
    }
    public class ReturnProvider: IReturnProvider
    {
        private readonly IReturnDataAccess _dataAccess;
        public ReturnProvider(IReturnDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public async Task<List<ReturnViewModel>> ReturnDetails()
        {
            return await _dataAccess.ReturnDetails();
        }
        public async Task<string> ReturnProducts(int purchaseID, int productID, int returnedQuantity)
        {
            return await _dataAccess.ReturnProducts(purchaseID, productID, returnedQuantity);
        }
        public async Task<string> DeleteReturnItemsByPurchaseID(int id)
        {
            return await _dataAccess.DeleteReturnItemsByPurchaseID(id);
        }
        public async Task<List<PurchaseDetailsModel>> GetPurchaseDetails()
        {
            return await _dataAccess.GetPurchaseDetails();
        }
    }
}