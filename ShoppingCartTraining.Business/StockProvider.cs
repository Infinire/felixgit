﻿using Microsoft.Extensions.Configuration;
using ShoppingCartTraining.Common.Models;
using ShoppingCartTraining.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ShoppingCartTraining.Business
{
    public interface IStockProvider
    {
        Task<List<StockModel>> GetStocks();
        Task<string> CreateStockDetails(int productId, int quantity);
        Task<string> UpdateStockDetailsById(int stockID, int quantity);
        Task<string> DeleteStockDetailsById(int stockId);
    }
    public class StockProvider : IStockProvider
    {
        private readonly IStockDataAccess _dataAccess;
        public StockProvider(IStockDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public async Task<List<StockModel>> GetStocks()
        {
            return await _dataAccess.GetStocks();
        }
        public async Task<string> CreateStockDetails(int productId,int quantity)
        {
            return await _dataAccess.CreateStockDetails(productId, quantity);
        }
        public async Task<string> UpdateStockDetailsById(int stockID, int quantity)
        {
            return await _dataAccess.UpdateStockDetailsById(stockID, quantity);
        }
        public async Task<string> DeleteStockDetailsById(int stockId)
        {
            return await _dataAccess.DeleteStockDetailsById(stockId);
        }
    }
}