﻿using ShoppingCartTraining.Common.Models;
using ShoppingCartTraining.DataAccess;
namespace ShoppingCartTraining.Business
{
    public interface IPurchaseProvider
    {
        Task<List<PurchaseModel>> GetPurchaseDetails();
        Task<string> CreatePurchaseDetails(int productID, int quantity, string createdBy);
        Task<string> DeletePurchaseDetailsById(int purchaseId);
    }
    public class PurchaseProvider : IPurchaseProvider
    {
        private readonly IPurchaseDataAccess _dataAccess;
        public PurchaseProvider(IPurchaseDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public async Task<List<PurchaseModel>> GetPurchaseDetails()
        {
            return await _dataAccess.GetPurchaseDetails();
        }
        public async Task<string> CreatePurchaseDetails(int productID, int quantity, string createdBy)
        {
            return await _dataAccess.CreatePurchaseDetails(productID, quantity, createdBy);
        }
        public async Task<string> DeletePurchaseDetailsById(int purchaseId)
        {
            return await _dataAccess.DeletePurchaseDetailsById(purchaseId);
        }
    }
}
