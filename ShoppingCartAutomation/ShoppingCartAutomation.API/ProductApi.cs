﻿using System.Text;
using Newtonsoft.Json;
using ShoppingCartAutomation.Models;

namespace ShoppingCartAutomation.API
{
    public class ProductApi
    {
        public readonly static HttpClient _httpClient = new HttpClient { Timeout = TimeSpan.FromMilliseconds(180000) };

        public async Task<string> InsertProduct(ProductModel product)
        {
            string url = "https://shoppingcartshunmugapriya.azurewebsites.net/Product/Add";
            var json = JsonConvert.SerializeObject(product);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(url, content);
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }

        public async Task<string> UpdateProduct(ProductModel product)
        {
            string url = "https://shoppingcartshunmugapriya.azurewebsites.net/Product/Edit";
            var json = JsonConvert.SerializeObject(product);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PutAsync(url, content);
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }

        public async Task<List<ProductModel>> GetAllProduct()
        {
            string url = "https://shoppingcartshunmugapriya.azurewebsites.net/Product/All";
            var response = await _httpClient.GetAsync(url);
            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ProductModel>>(result);
        }

        public async Task<string> DeleteProduct(int productID)
        {
            string url = "https://shoppingcartshunmugapriya.azurewebsites.net/Product/Delete/" + productID;
            var response = await _httpClient.DeleteAsync(url);
            var result = await response.Content.ReadAsStringAsync();
            return result;
        }
    }
}