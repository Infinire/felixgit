﻿using ShoppingCartAutomation.Models;
using ShoppingCartAutomationSpecflow.Core.Database;

namespace ShoppingCartAutomation.Database
{
    public class DatabaseQuery
    {
        public readonly QueryExecutor _queryExecutor;
        private string _connectionString = "Server=infiniresqlserver.database.windows.net;User=shunmugapriya;Password=Priya*123;Database=ShoppingCart";

        public DatabaseQuery(QueryExecutor queryExecutor)
        {
            _queryExecutor = queryExecutor;
        }

        public async Task<ProductModel> GetProductDetailsByProductName(string productName)
        {
            var parameters = new { ProductName = productName };
            var query = $"SELECT * FROM dbo.Product WITH (nolock) WHERE ProductName = @ProductName ORDER BY ProductID";
            return await _queryExecutor.GetDataAsync<ProductModel>(query, _connectionString, parameters);
        }
    }
}