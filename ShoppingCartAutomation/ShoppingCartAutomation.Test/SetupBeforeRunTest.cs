﻿using BoDi;
namespace ShoppingCartAutomation.Test
{
    [Binding]
    public static class SetupBeforeRunningTest
    {
        static IConfiguration config;

        [BeforeTestRun]
        public static void CreateConfig(IObjectContainer _objectContainer)
        {
            if (config == null)
            {
                var builder = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.Test.json");
                config = builder.Build();
            }
            _objectContainer.RegisterInstanceAs(config);
        }

    }
}