﻿using NUnit.Framework;
using ShoppingCartAutomation.Models;
using ShoppingCartAutomation.Services;
using ShoppingCartAutomation.Test.Support;

namespace ShoppingCartAutomation.Test.StepDefinitions
{
    [Binding]
    public class ProductStepDefinitions
    {
        readonly ScenarioContext _scenarioContext;
        readonly ProductService _productService;
        readonly Utility _utility;

        public ProductStepDefinitions(ScenarioContext scenarioContext, ProductService productService, Utility utility)
        {
            _scenarioContext = scenarioContext;
            _productService = productService;
            _utility = utility;
        }

        // Add  A Product
        [Given(@"I have a product details")]
        public void GivenIHaveAProductDetails()
        {
            var productDetail = new ProductModel();
            productDetail.ProductName = _utility.RandomStringGenerator(6);
            productDetail.UnitPrice = 10;
            productDetail.UnitDiscount = 5;
            productDetail.CreatedBy = "Automation";

            _scenarioContext.Set(productDetail, "AddProduct");
        }

        [Given(@"I call add product API")]
        [When(@"I call add product API")]
        public async Task WhenICallAddProductAPI()
        {
            var productObject = _scenarioContext.Get<ProductModel>("AddProduct");
            var result = await _productService.InsertProduct(productObject);
            _scenarioContext.Set(result, "productResponse");
        }

        [Given(@"the response got '([^']*)' message")]
        [Then(@"the response got '([^']*)' message")]
        public void ThenTheResponseGotMessage(string responseMessage)
        {
            var response = _scenarioContext.Get<string>("productResponse");
            Assert.AreEqual(responseMessage, response);
        }

        [Given(@"The product added into the table")]
        [Then(@"The product added into the table")]
        public async Task ThenTheProductAddedIntoTheTable()
        {
            var product = _scenarioContext.Get<ProductModel>("AddProduct");
            var expectedResult = await _productService.GetProductByProductName(product.ProductName);
            _scenarioContext.Set(expectedResult, "ProductSavedDetails");
            Assert.AreEqual(product.ProductName, expectedResult.ProductName);
        }

        //Edit a product
        [When(@"I call edit product API")]
        public async Task WhenICallEditProductAPI()
        {
            var productdetails = _scenarioContext.Get<ProductModel>("ProductSavedDetails");

            productdetails.ProductName = _utility.RandomStringGenerator(6);
            productdetails.UnitPrice = 20;
            productdetails.UnitDiscount = 10;
            productdetails.ModifiedBy = "Automation Modified";

            _scenarioContext.Set(productdetails, "ProductModified");

            var result = await _productService.UpdateProduct(productdetails);
            _scenarioContext.Set(result, "productResponse");
        }

        [Then(@"the product is updated in the table")]
        public async Task ThenTheProductIsUpdatedInTheTable()
        {
            var product = _scenarioContext.Get<ProductModel>("ProductModified");
            var expectedResult = await _productService.GetProductByProductName(product.ProductName);
            _scenarioContext.Set(expectedResult, "ProductUpdatedDetails");
            Assert.AreEqual(product.ProductName, expectedResult.ProductName);
            Assert.AreEqual(product.UnitPrice, expectedResult.UnitPrice);
            Assert.AreEqual(product.UnitDiscount, expectedResult.UnitDiscount);
            Assert.AreEqual(product.ModifiedBy, expectedResult.ModifiedBy);
        }

        //Get Product
        [When(@"I call get product API")]
        [Then(@"I call get product API")]
        public async Task WhenICallGetProductAPI()
        {
            var product = _scenarioContext.Get<ProductModel>("ProductSavedDetails");
            var productLModelList = await _productService.GetAllProduct();
            var result = productLModelList.Where(r => r.ProductID == product.ProductID).FirstOrDefault();
            _scenarioContext.Set(result, "FoundAProduct");
        }

        [Then(@"the product is exist in the list")]
        public void ThenTheProductIsConfirmedInTheList()
        {
            var actualResult = _scenarioContext.Get<ProductModel>("FoundAProduct");
            var expectedResult = _scenarioContext.Get<ProductModel>("ProductSavedDetails");
            Assert.NotNull(actualResult);
            Assert.Greater(actualResult.ProductID, 0);
            Assert.AreEqual(expectedResult.ProductID, actualResult.ProductID);
        }

        //Delte a product
        [When(@"I call delete product API")]
        public async Task WhenICallDeleteProductAPI()
        {
            var actualResult = _scenarioContext.Get<ProductModel>("ProductSavedDetails");
            var response = await _productService.DeleteProduct(actualResult.ProductID);
            _scenarioContext.Set(response, "productResponse");
        }

        [Then(@"the product removed from the table")]
        public async void ThenTheProductRemovedFromTheTable()
        {
            var actualResult = _scenarioContext.Get<ProductModel>("ProductSavedDetails");
            var expectedResult = await _productService.GetProductByProductName(actualResult.ProductName);
            Assert.Null(expectedResult);
        }
    }
}
