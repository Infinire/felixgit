﻿Feature: Product

This product feature describe Add/Update/View/Delete the products.

@AddingProduct
Scenario: Add product
	Given I have a product details
	When I call add product API
	Then the response got 'Product Saved Successfully' message
	And The product added into the table

@EditingProduct
Scenario: Edit Product
	Given I have a product details
	And I call add product API
	And the response got 'Product Saved Successfully' message
	And The product added into the table
	When I call edit product API
	Then the response got 'Product Updated Successfully' message
	And the product is updated in the table

@GetProduct
Scenario: Get Product
	Given I have a product details
	And I call add product API
	And the response got 'Product Saved Successfully' message
	And The product added into the table
	When I call get product API
	Then the product is exist in the list

@DeleteProduct
Scenario: Delete Product
	Given I have a product details
	And I call add product API
	And the response got 'Product Saved Successfully' message
	And The product added into the table
	When I call delete product API
	Then the response got 'Deleted Successfully' message
	And the product removed from the table