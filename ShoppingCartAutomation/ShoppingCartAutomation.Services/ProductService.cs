﻿using ShoppingCartAutomation.Models;
using ShoppingCartAutomation.API;
using ShoppingCartAutomation.Database;

namespace ShoppingCartAutomation.Services
{
    public class ProductService
    {
        readonly ProductApi _productApi;
        readonly DatabaseQuery _databaseQuery;

        public ProductService(ProductApi productApi, DatabaseQuery databaseQuery)
        {
            _productApi = productApi;
            _databaseQuery = databaseQuery;
        }

        public async Task<string> InsertProduct(ProductModel product)
        {
            return await _productApi.InsertProduct(product);
        }

        public async Task<string> UpdateProduct(ProductModel product)
        {
            return await _productApi.UpdateProduct(product);
        }
        public async Task<ProductModel> GetProductByProductName(string productName)
        {
            return await _databaseQuery.GetProductDetailsByProductName(productName);
        }

        public async Task<List<ProductModel>> GetAllProduct()
        {
            return await _productApi.GetAllProduct();
        }

        public async Task<string> DeleteProduct(int productID)
        {
            return await _productApi.DeleteProduct(productID);
        }
    }
}
