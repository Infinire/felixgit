﻿using Dapper;
using System.Data.SqlClient;

namespace ShoppingCartAutomationSpecflow.Core.Database
{
    public class QueryExecutor
    {
        public const int COMMANDTIMEOUT = 30;

        public async Task<T> GetDataAsync<T>(string query, string connectionString, object parameters = null, int? commandTimeout = null)
        {
            T result = default(T);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                if (parameters != null)
                {
                    result = await connection.QueryFirstOrDefaultAsync<T>(query, parameters, commandTimeout: commandTimeout ?? COMMANDTIMEOUT);
                }
                else
                {
                    result = await connection.QueryFirstOrDefaultAsync<T>(query, commandTimeout: commandTimeout ?? COMMANDTIMEOUT);
                }
            }
            return result;
        }
    }
}
