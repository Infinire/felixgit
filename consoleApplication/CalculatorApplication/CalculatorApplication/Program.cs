﻿using System;

namespace CalculatorApplication
{
    class Program
    {
        public static void Main(string[] args)
        {
            bool endApp = false;

            Console.WriteLine("Console calculator in C#\r");
            Console.WriteLine("-------------------------\n");

            while (!endApp)
            {
                string number1 = "";
                string number2 = "";
                double result = 0;

                Console.WriteLine("Type a number, and then press enter: ");
                number1 = Console.ReadLine();

                double clean1 = 0;
                while (!double.TryParse(number1, out clean1))
                {
                    Console.WriteLine("This is not a valid number. Please enter an integer value: ");
                    number1 = Console.ReadLine();
                }

                Console.WriteLine("Type a number, and then press enter: ");
                number2 = Console.ReadLine();

                double clean2 = 0;
                while (!double.TryParse(number2, out clean2))
                {
                    Console.WriteLine("This is not a valid number. Please enter an integer value: ");
                    number2 = Console.ReadLine();
                }

                Console.WriteLine("Choose a operator from following list: ");
                Console.WriteLine("\ta - Add");
                Console.WriteLine("\ts - Subtract");
                Console.WriteLine("\tm - Multiply");
                Console.WriteLine("\td - Division");
                Console.WriteLine("Choose from the Option list : ");

                string op = Console.ReadLine();

                try
                {
                    result = Calculator.DoOperation(clean1, clean2, op);
                    if (double.IsNaN(result))
                    {
                        Console.WriteLine("This operation result when an mathematical error!..");
                    }
                    else
                    {
                        Console.WriteLine("The Result:" + result);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("An exception error occur do not match the details: \n" + e.Message);
                }
                Console.WriteLine("---------------------------");

                Console.Write("Press 'n' and Enter to close the app, or press any other key and Enter to continue:");
                if (Console.ReadLine() == "n") endApp = true;

                Console.WriteLine("\n");
            }
            return;
        }
    }

    class Calculator
    {
        public static double DoOperation(double num1, double num2, string op)
        {
            double result = double.NaN;

            switch (op)
            {
                case "a":
                    result = num1 + num2;
                    break;
                case "s":
                    result = num1 - num2;
                    break;
                case "m":
                    result = num1 * num2;
                    break;
                case "d":
                    if (num2 != 0)
                    {
                        result = num1 / num2;
                    }
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}
