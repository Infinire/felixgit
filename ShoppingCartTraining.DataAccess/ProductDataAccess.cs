﻿using Microsoft.Extensions.Configuration;
using ShoppingCartTraining.Common.Models;
using System.Data;
namespace ShoppingCartTraining.DataAccess
{
    public interface IProductDataAccess
    {
        Task<List<ProductModel>> GetProducts();
        Task<List<ProductModel>> GetProductById(int productId);
        Task<List<ProductModel>> CreateProduct(ProductModel data);
        Task<List<ProductModel>> UpdateProduct(ProductModel data);
        Task<bool> DeleteProduct(int productId);
    }
    public class ProductDataAccess : IProductDataAccess
    {
        private readonly IConfiguration _configuration;
        private readonly ICommonDataAccess _dataAccess;

        public ProductDataAccess(IConfiguration configuration, ICommonDataAccess dataAccess)
        {
            _configuration = configuration;
            _dataAccess = dataAccess;
        }
        public async Task<List<ProductModel>> GetProducts()
        {
            var products = new List<ProductModel>();
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSet = await _dataAccess.ExecuteDataSet("SELECT * FROM Products", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    var product = new ProductModel()
                    {
                        ProductID = (int)dataRow["ProductID"],
                        ProductName = dataRow["ProductName"].ToString(),
                        UnitPrice = (decimal)dataRow["UnitPrice"],
                        UnitDiscount = (decimal)dataRow["UnitDiscount"],
                        CreatedBy = dataRow["CreatedBy"].ToString(),
                        ModifiedBy = dataRow["ModifiedBy"].ToString(),
                        CreatedDate = dataRow["CreatedDate"].ToString(),
                        ModifiedDate = dataRow["ModifiedDate"].ToString()
                    };
                    products.Add(product);
                }
            }
            return products;
        }
        public async Task<List<ProductModel>> GetProductById(int productId)
        {
            var products = new List<ProductModel>();
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSet = await _dataAccess.ExecuteDataSet($"SELECT * FROM Products WHERE ProductID={productId}", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    var product = new ProductModel()
                    {
                        ProductID = (int)dataRow["ProductID"],
                        ProductName = dataRow["ProductName"].ToString(),
                        UnitPrice = (decimal)dataRow["UnitPrice"],
                        UnitDiscount = (decimal)dataRow["UnitDiscount"],
                        CreatedBy = dataRow["CreatedBy"].ToString()
                    };
                    products.Add(product);
                }
            }
            return products;
        }
        public async Task<List<ProductModel>> CreateProduct(ProductModel data)
        {
            var products = new List<ProductModel>();
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSet = await _dataAccess.ExecuteDataSet($"SELECT * FROM Products WHERE ProductName='{data.ProductName}'", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                throw new Exception("Product name already exists!");
            }
            var result = _dataAccess.ExecuteNonQuery("INSERT INTO Products(ProductName, UnitPrice, UnitDiscount, CreatedBy) " + 
                $"VALUES('{data.ProductName}', {data.UnitPrice}, {data.UnitDiscount}, '{data.CreatedBy}')", connection);
            var dataSetProducts = await _dataAccess.ExecuteDataSet("SELECT TOP(1) * FROM Products ORDER BY 1 DESC", connection);
            if (dataSetProducts != null && dataSetProducts.Tables.Count > 0 && dataSetProducts.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dataRow in dataSetProducts.Tables[0].Rows)
                {
                    var product = new ProductModel()
                    {
                        ProductID = (int)dataRow["ProductID"],
                        ProductName = dataRow["ProductName"].ToString(),
                        UnitPrice = (decimal)dataRow["UnitPrice"],
                        UnitDiscount = (decimal)dataRow["UnitDiscount"],
                        CreatedBy = dataRow["CreatedBy"].ToString()
                    };
                    products.Add(product);
                }
            }
            return products;
        }
        public async Task<List<ProductModel>> UpdateProduct(ProductModel data)
        {
            var products = new List<ProductModel>();
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var result = _dataAccess.ExecuteNonQuery($"UPDATE Products SET ProductName='{data.ProductName}', UnitPrice={data.UnitPrice}, UnitDiscount={data.UnitDiscount}, CreatedBy='{data.CreatedBy}', ModifiedBy='{data.ModifiedBy}', ModifiedDate='{DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")}' WHERE ProductID={data.ProductID}", connection);
            var dataSet = await _dataAccess.ExecuteDataSet($"SELECT * FROM Products WHERE ProductID={data.ProductID}", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    var product = new ProductModel()
                    {
                        ProductID = (int)dataRow["ProductID"],
                        ProductName = dataRow["ProductName"].ToString(),
                        UnitPrice = (decimal)dataRow["UnitPrice"],
                        UnitDiscount = (decimal)dataRow["UnitDiscount"],
                        CreatedBy = dataRow["CreatedBy"].ToString(),
                        ModifiedBy = dataRow["ModifiedBy"].ToString(),
                        CreatedDate = dataRow["CreatedDate"].ToString(),
                        ModifiedDate = dataRow["ModifiedDate"].ToString()
                    };
                    products.Add(product);
                }
            }
            return products;
        }
        public async Task<bool> DeleteProduct(int productId)
        {
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSet = await _dataAccess.ExecuteDataSet($"SELECT * FROM Products WHERE ProductID={productId}", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count == 0)
            {
                throw new Exception("Product is not found! It may deleted OR Not created yet!");
            }
            var dataSetProducts = await _dataAccess.ExecuteDataSet($"SELECT * FROM Products P INNER JOIN Purchase PC ON P.ProductID = PC.ProductID INNER JOIN Returns R ON P.ProductID = R.ProductID WHERE P.ProductID={productId}", connection);
            if (dataSetProducts != null && dataSetProducts.Tables.Count > 0 && dataSetProducts.Tables[0].Rows.Count > 0)
            {
                throw new Exception("Product has purchased or returned! Can't delete the record now!");
            }
            var result = _dataAccess.ExecuteNonQuery($"DELETE FROM Products WHERE ProductID={productId}", connection);
            return true;
        }
    }
}