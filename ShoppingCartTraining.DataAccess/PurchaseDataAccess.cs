﻿using Microsoft.Extensions.Configuration;
using ShoppingCartTraining.Common.Models;
using System.Data;
namespace ShoppingCartTraining.DataAccess
{
    public interface IPurchaseDataAccess
    {
        Task<List<PurchaseModel>> GetPurchaseDetails();
        Task<string> CreatePurchaseDetails(int productID, int quantity, string createdBy);
        Task<string> DeletePurchaseDetailsById(int purchaseId);
    }
    public class PurchaseDataAccess : IPurchaseDataAccess
    {
        private readonly IConfiguration _configuration;
        private readonly ICommonDataAccess _dataAccess;
        public PurchaseDataAccess(IConfiguration configuration, ICommonDataAccess dataAccess)
        {
            _configuration = configuration;
            _dataAccess = dataAccess;
        }
        public async Task<List<PurchaseModel>> GetPurchaseDetails()
        {
            var purchases = new List<PurchaseModel>();
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSet = await _dataAccess.ExecuteDataSet("SELECT " +
                                "P.ProductName, " +
                                "P.ProductID, " +
                                "PC.PurchaseID, " +
                                "PC.PurchasedDate, " +
                                "P.UnitPrice, " +
                                "P.UnitDiscount, " +
                                "(PC.Quantity - ISNULL(R.ReturnedQuantity, 0)) AS Quantity, " +
                                "(PC.TotalDiscount - ISNULL(R.ReturnedQuantity, 0)) AS TotalDiscount, " +
                                "(PC.TotalAmount - ISNULL(R.ReturnedTotalAmount, 0)) AS TotalAmount " +
                                "FROM " +
                                "Purchase AS PC " +
                                "INNER JOIN Products P ON P.ProductID = PC.ProductID " +
                                "LEFT JOIN Returns R ON PC.PurchaseID = R.PurchaseID", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    purchases.Add(new PurchaseModel()
                    {
                        ProductID = (int)dataRow["ProductID"],
                        ProductName = (string)dataRow["ProductName"],
                        PurchaseID = (int)dataRow["PurchaseID"],
                        UnitPrice = (decimal)dataRow["UnitPrice"],
                        UnitDiscount = (decimal)dataRow["UnitDiscount"],
                        Quantity = (int)dataRow["Quantity"],
                        TotalDiscount = (decimal)dataRow["TotalDiscount"],
                        TotalAmount = (decimal)dataRow["TotalAmount"],
                    });
                }
            }
            return purchases;
        }

        public async Task<string> CreatePurchaseDetails(int productID, int quantity, string createdBy)
        {
            var stocks = new List<PurchaseModel>();
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSet = await _dataAccess.ExecuteDataSet($"SELECT * FROM Products WHERE ProductID={productID}", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count == 0)
            {
                return "Product is not exist! Please verify!";
            }
            var dataSetPurchase = await _dataAccess.ExecuteDataSet($"SELECT * FROM Stocks WHERE ProductID='{productID}'", connection);
            if (dataSetPurchase != null && dataSetPurchase.Tables.Count > 0 && dataSetPurchase.Tables[0].Rows.Count == 0)
            {
                return "Product is not exist in stock! Please verify!";
            }
            var stockQuantity = (int)dataSetPurchase.Tables[0].Rows[0]["Quantity"];
            if (stockQuantity < quantity)
            {
                return $"Sorry! The Stock has only {stockQuantity} quantity";
            }
            var UnitPrice = (decimal)dataSet.Tables[0].Rows[0]["UnitPrice"];
            var UnitDiscount = (decimal)dataSet.Tables[0].Rows[0]["UnitDiscount"] / 100;
            var TotalDiscount = quantity * (UnitPrice * UnitDiscount);
            var TotalAmount = (quantity * UnitPrice) - TotalDiscount;
            var result = _dataAccess.ExecuteNonQuery($"INSERT INTO Purchase(ProductID, Quantity, UnitPrice, UnitDiscount, TotalDiscount, TotalAmount, PurchasedDate, CreatedBy, CreatedDate) " +
                        $"VALUES({productID}, {quantity}, {UnitPrice}, {UnitDiscount}, {TotalDiscount}, {TotalAmount}, '{DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")}', '{createdBy}', '{DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")}')", connection);
            var RemaingQuantity = (int)dataSetPurchase.Tables[0].Rows[0]["Quantity"] - quantity;
            _dataAccess.ExecuteNonQuery($"UPDATE Stocks SET Quantity={RemaingQuantity} WHERE ProductID={productID}", connection);
            return "Successfully purchased!";
        }

        public async Task<string> DeletePurchaseDetailsById(int purchaseId)
        {
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSet = await _dataAccess.ExecuteDataSet($"SELECT * FROM Purchase WHERE PurchaseID={purchaseId}", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count == 0)
            {
                return "Sorry! The purchase id is not exist.";
            }
            var dataSetPurchase = await _dataAccess.ExecuteDataSet($"SELECT * FROM Returns WHERE PurchaseID={purchaseId}", connection);
            if (dataSetPurchase != null && dataSetPurchase.Tables.Count > 0 && dataSetPurchase.Tables[0].Rows.Count > 0)
            {
                _dataAccess.ExecuteNonQuery($"DELETE FROM Returns WHERE PurchaseID={purchaseId}", connection);
            }
            var dataSetPurchaseReturn = await _dataAccess.ExecuteDataSet($"SELECT * FROM Stocks WHERE ProductID={(int)dataSet.Tables[0].Rows[0]["ProductID"]}", connection);
            _dataAccess.ExecuteNonQuery($"DELETE FROM Purchase WHERE PurchaseID={purchaseId}", connection);
            var Quantity = (int)dataSetPurchaseReturn.Tables[0].Rows[0]["Quantity"] + (int)dataSet.Tables[0].Rows[0]["Quantity"];
            _dataAccess.ExecuteNonQuery($"UPDATE Stocks SET Quantity={Quantity} WHERE ProductID={(int)dataSet.Tables[0].Rows[0]["ProductID"]}", connection);
            return "Deleted Successfully!";
        }
    }
}