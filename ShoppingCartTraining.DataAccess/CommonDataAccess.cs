﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingCartTraining.DataAccess
{
    public interface ICommonDataAccess
    {
        Task<DataSet> ExecuteDataSet(string query, string connection);
        int ExecuteNonQuery(string query, string connection);
    }
    public class CommonDataAccess: ICommonDataAccess
    {
        public async Task<DataSet> ExecuteDataSet(string query, string connection)
        {
            DataSet dataset = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlConnection conn = new SqlConnection(connection);
            using SqlCommand command = new SqlCommand(query, conn);
            conn.Open();
            adapter.SelectCommand = command;
            adapter.Fill(dataset);
            conn.Close();
            return dataset;
        }
        public int ExecuteNonQuery(string query, string connection)
        {
            int result = 0;
            SqlConnection conn = new SqlConnection(connection);
            using SqlCommand command = new SqlCommand(query, conn);
            conn.Open();
            result = command.ExecuteNonQuery();
            conn.Close();
            return result;
        }
    }
}
