﻿using Microsoft.Extensions.Configuration;
using ShoppingCartTraining.Common.Models;
using System.Data;
namespace ShoppingCartTraining.DataAccess
{
    public interface IReturnDataAccess
    {
        Task<List<ReturnViewModel>> ReturnDetails();
        Task<string> ReturnProducts(int purchaseID, int productID, int returnedQuantity);
        Task<string> DeleteReturnItemsByPurchaseID(int id);
        Task<List<PurchaseDetailsModel>> GetPurchaseDetails();
    }
    public class ReturnDataAccess : IReturnDataAccess
    {
        private readonly IConfiguration _configuration;
        private readonly ICommonDataAccess _dataAccess;
        public ReturnDataAccess(ICommonDataAccess dataAccess, IConfiguration configuration)
        {
            _configuration = configuration;
            _dataAccess = dataAccess;
        }
        public async Task<List<ReturnViewModel>> ReturnDetails()
        {
            var productReturns = new List<ReturnViewModel>();
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSet = await _dataAccess.ExecuteDataSet("SELECT " +
                            "R.ReturnID, R.ProductId, P.ProductName, R.PurchaseID, R.ReturnedDate, " +
                            "R.ReturnedQuantity, PC.UnitPrice, PC.UnitDiscount, R.ReturnedTotalDiscount, R.ReturnedTotalAmount " +
                            "FROM " +
                            "Returns R " +
                            "INNER JOIN Purchase PC ON R.PurchaseID = PC.PurchaseID " +
                            "INNER JOIN Products P ON PC.ProductID = P.ProductID", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    productReturns.Add(new ReturnViewModel()
                    {
                        ProductID = (int)dataRow["ProductID"],
                        ProductName = (string)dataRow["ProductName"],
                        PurchaseID = (int)dataRow["PurchaseID"],
                        ReturnedDate = dataRow["ReturnedDate"].ToString(),
                        ReturnedQuantity = (int)dataRow["ReturnedQuantity"],
                        ReturnedTotalDiscount = (decimal)dataRow["ReturnedTotalDiscount"],
                        ReturnedTotalAmount = (decimal)dataRow["ReturnedTotalAmount"]
                    });
                }
            }
            return productReturns;
        }

        public async Task<string> ReturnProducts(int purchaseID, int productID, int returnedQuantity)
        {
            var productReturns = new List<ReturnModel>();
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSetPurchase = await _dataAccess.ExecuteDataSet($"SELECT * FROM Purchase WHERE PurchaseID={purchaseID} AND ProductID={productID}", connection);
            if (dataSetPurchase != null && dataSetPurchase.Tables.Count > 0 && dataSetPurchase.Tables[0].Rows.Count == 0)
            {
                return "Sorry! The purchase id does not exist!";
            }
            var dataSetProducts = await _dataAccess.ExecuteDataSet($"SELECT * FROM Products WHERE ProductID={productID}", connection);
            if (dataSetProducts != null && dataSetProducts.Tables.Count > 0 && dataSetProducts.Tables[0].Rows.Count == 0)
            {
                return "Sorry! The product id does not exist!";
            }
            var UnitPrice = (decimal)dataSetProducts.Tables[0].Rows[0]["UnitPrice"];
            var UnitDiscount = ((decimal)dataSetProducts.Tables[0].Rows[0]["UnitDiscount"] / 100);
            var ReturnedTotalDiscount = returnedQuantity * (UnitPrice * UnitDiscount);
            var ReturnedTotalAmount = (returnedQuantity * UnitPrice) - ReturnedTotalDiscount;
            var result = _dataAccess.ExecuteNonQuery($"INSERT INTO Returns(PurchaseID, ProductID, ReturnedQuantity, ReturnedTotalDiscount, ReturnedTotalAmount, ReturnedDate, CreatedBy) " +
                         $"VALUES({purchaseID}, {productID}, {returnedQuantity}, {ReturnedTotalDiscount}, {ReturnedTotalAmount}, '{DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")}', 'Vijay Felix Raj C')", connection);
            return "The product returned successfully!";
        }

        public async Task<string> DeleteReturnItemsByPurchaseID(int id)
        {
            var productReturns = new List<ReturnModel>();
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSetPurchase = await _dataAccess.ExecuteDataSet($"SELECT * FROM Purchase WHERE PurchaseID={id}", connection);
            if (dataSetPurchase != null && dataSetPurchase.Tables.Count > 0 && dataSetPurchase.Tables[0].Rows.Count == 0)
            {
                return "Sorry! Purchase details is not found!";
            }
            _dataAccess.ExecuteNonQuery($"DELETE FROM Returns WHERE PurchaseID={id}", connection);
            return "Deleted Successfully!";
        }

        public async Task<List<PurchaseDetailsModel>> GetPurchaseDetails()
        {
            var ListPurchase = new List<PurchaseDetailsModel>();
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSet = await _dataAccess.ExecuteDataSet("SELECT P.ProductName, P.ProductID, PC.PurchasedDate, PC.Quantity, PC.TotalDiscount, PC.TotalAmount, " +
                    "R.ReturnedDate, R.ReturnedQuantity, R.ReturnedTotalDiscount, R.ReturnedTotalAmount " +
                    "FROM Purchase AS PC " +
                    "INNER JOIN Products P ON P.ProductID = PC.ProductID " +
                    "LEFT JOIN Returns R ON PC.PurchaseID = R.PurchaseID", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    ListPurchase.Add(new PurchaseDetailsModel()
                    {
                        ProductID = (int)(!Convert.IsDBNull(dataRow["ProductID"]) ? dataRow["ProductID"] : null),
                        ProductName = (string)dataRow["ProductName"],
                        PurchasedDate = dataRow["PurchasedDate"].ToString(),
                        ReturnedDate = dataRow["ReturnedDate"].ToString(),
                        Quantity = (int)(!Convert.IsDBNull(dataRow["Quantity"]) ? dataRow["Quantity"] : null),
                        TotalDiscount = (decimal)dataRow["TotalDiscount"],
                        TotalAmount = (decimal)dataRow["TotalAmount"],
                        ReturnedQuantity = (int)(!Convert.IsDBNull(dataRow["ReturnedQuantity"]) ? dataRow["ReturnedQuantity"] : 0),
                        ReturnedTotalDiscount = (decimal)(!Convert.IsDBNull(dataRow["ReturnedTotalDiscount"]) ? dataRow["ReturnedTotalDiscount"] : 0M),
                        ReturnedTotalAmount = (decimal)(!Convert.IsDBNull(dataRow["ReturnedTotalAmount"]) ? dataRow["ReturnedTotalAmount"] : 0M)
                    });
                }
            }
            return ListPurchase;
        }
    }
}