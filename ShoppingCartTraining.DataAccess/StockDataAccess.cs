﻿using Microsoft.Extensions.Configuration;
using ShoppingCartTraining.Common.Models;
using System.Data;
using System.Data.SqlClient;
namespace ShoppingCartTraining.DataAccess
{
    public interface IStockDataAccess
    {
        Task<List<StockModel>> GetStocks();
        Task<string> CreateStockDetails(int productId, int quantity);
        Task<string> UpdateStockDetailsById(int stockID, int quantity);
        Task<string> DeleteStockDetailsById(int stockId);
    }
    public class StockDataAccess: IStockDataAccess
    {
        private readonly ICommonDataAccess _dataAccess;
        private readonly IConfiguration _configuration;
        public StockDataAccess(ICommonDataAccess commonDataAccess, IConfiguration configuration)
        {
            _dataAccess = commonDataAccess;
            _configuration = configuration;
        }

        public async Task<List<StockModel>> GetStocks()
        {
            var stocks = new List<StockModel>();
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSet = await _dataAccess.ExecuteDataSet("SELECT s.StockID, s.ProductID, p.ProductName, p.UnitPrice, s.Quantity, (p.UnitPrice * s.Quantity) as TotalAmout FROM Stocks s INNER JOIN Products p ON s.ProductID = p.ProductID", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    stocks.Add(new StockModel()
                    {
                        StockID = (int)dataRow["StockID"],
                        ProductID = (int)dataRow["ProductID"],
                        ProductName = dataRow["ProductName"].ToString(),
                        Quantity = (int)dataRow["Quantity"],
                        UnitPrice = (decimal)dataRow["UnitPrice"],
                        TotalAmout = (decimal)dataRow["TotalAmout"]
                    });
                }
            }
            return stocks;
        }

        public async Task<string> CreateStockDetails(int productId, int quantity)
        {
            string message = "";
            var connection = _configuration.GetConnectionString("DefaultConnection");
            var dataSet = await _dataAccess.ExecuteDataSet($"SELECT * FROM Products WHERE ProductID={productId}", connection);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count == 0)
            {
                message = "Sorry! Please confirm the product exist before creating the stock.";
            }
            var dataSetstock = await _dataAccess.ExecuteDataSet($"SELECT * FROM Stocks WHERE ProductID={productId}", connection);
            if (dataSetstock != null && dataSetstock.Tables.Count > 0 && dataSetstock.Tables[0].Rows.Count > 0)
            {
                var StockQuantity = (int)dataSetstock.Tables[0].Rows[0]["Quantity"] + quantity;
                var result = _dataAccess.ExecuteNonQuery($"UPDATE Stocks SET Quantity={StockQuantity}, ModifiedBy='Felix', ModifiedDate='{DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")}' WHERE ProductID={productId}", connection);
                message = "Successfully updated quantity. Because the product already exist!";
            }
            else
            {
                var insertResult = _dataAccess.ExecuteNonQuery($"INSERT INTO Stocks(ProductID, Quantity, CreatedBy) VALUES({productId}, {quantity}, 'Felix')", connection);
                message = "Inserted Successfully!.";
            }
            return message;
        }

        public async Task<string> UpdateStockDetailsById(int stockID, int quantity)
        {
            string message = "";
            var connection = _configuration.GetConnectionString("DefaultConnection");
            _dataAccess.ExecuteNonQuery($"UPDATE Stocks SET Quantity={quantity}, ModifiedBy='Felix', ModifiedDate='{DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")}' WHERE StockID={stockID}", connection);
            message = "Successfully updated!";
            return message;
        }

        public async Task<string> DeleteStockDetailsById(int stockId)
        {
            string message = "";
            var connection = _configuration.GetConnectionString("DefaultConnection");
            _dataAccess.ExecuteNonQuery($"DELETE FROM Stocks WHERE StockID={stockId}", connection);
            message = "Deleted successfully!";
            return message;
        }
    }
}
